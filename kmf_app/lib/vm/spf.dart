// ignore_for_file: avoid_print, prefer_interpolation_to_compose_strings

import 'package:shared_preferences/shared_preferences.dart';

class SP {
  static Future save(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    print('Saved : ' + key + " Value : " + value);
  }

  static Future getToken() async {
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token');
    return token;
  }

  static Future getOthers(other) async {
    final prefs = await SharedPreferences.getInstance();
    String? x = prefs.getString(other);
    return x;
  }
}
