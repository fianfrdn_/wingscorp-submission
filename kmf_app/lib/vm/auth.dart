// ignore_for_file: non_constant_identifier_names

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kmf_app/core/endpoint.dart';
import 'package:kmf_app/vm/spf.dart';

class AuthVM {
  static String url = Server.ept;

  static Future login(String username, password) async {
    var endpoint = Uri.parse(url + "UO5E33SE2I-WT62CP6WAP-WHVPYH10E6/login");
    final response = await http
        .post(endpoint, body: {'email': username, 'password': password});
    try {
      var res = json.decode(response.body);
      if (response.statusCode == 401) {
        return res;
      } else if (response.statusCode == 203) {
        var user = res['msg']['user'];
        SP.save('token', res['msg']['token']);
        return res;
      }
    } on Exception catch (e) {
      return 'Error server : ' + e.toString();
    }
  }

  static Future product() async {
    var endpoint = Uri.parse(url + "UO5E33SE2I-WT62CP6WAP-WHVPYH10E6/product");
    var token = await SP.getToken();
    final response = await http.get(
        headers: {'Authorization': 'Bearer ' + token.toString()}, endpoint);
    try {
      var res = json.decode(response.body);
      return res;
    } on Exception catch (e) {
      return 'Error server : ' + e.toString();
    }
  }

  static Future checkout(List<String> productCode, qty, String total) async {
    var endpoint = Uri.parse(url + "UO5E33SE2I-WT62CP6WAP-WHVPYH10E6/checkout");
    var token = await SP.getToken();
    print(productCode.toString());
    final response = await http.post(endpoint, headers: {
      'Authorization': 'Bearer ' + token.toString()
    }, body: {
      'productCode': productCode.toString(),
      'qty': qty.toString(),
      'total': total.toString(),
    });
    // var res = json.decode(response.body);
    // print("res $res");
    return 'res';
    try {} on Exception catch (e) {
      return 'Error server : ' + e.toString();
    }
  }

  static Future checkAuth() async {
    var token = await SP.getToken();
    var endpoint = Uri.parse(url + "UO5E33SE2I-WT62CP6WAP-WHVPYH10E6/me");
    final response = await http.get(endpoint,
        headers: {'Authorization': 'Bearer ' + token.toString()});
    print("endpoint $endpoint");
    try {
      var res = json.decode(response.body);
      if (response.statusCode == 200) {
        return 'login';
      } else if (response.statusCode == 203) {
        return 'home';
      }
    } on Exception catch (e) {
      return 'login';
    }
  }

  static Future saver(String token, userid, name, email, kip) async {
    SP.save('token', token);
    SP.save('user_id', userid);
    SP.save('nama', name);
    SP.save('email', email);
    SP.save('kip', kip);
  }
}
