import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kmf_app/core/endpoint.dart';
import 'package:kmf_app/vm/spf.dart';

class AnggotaVM {
  static String url = Server.ept;
  static Future updateUser({
    required String id,
    required name,
    required email,
    required nik,
    required nohp,
    required ktp,
    required alamat,
  }) async {
    var token = await SP.getToken();
    var endpoint =
        Uri.parse(url + "UO5E33SE2I-WT62CP6WAP-WHVPYH10E6/user/update/profile");
    print("EAP : $endpoint");
    final response = await http.post(endpoint, headers: {
      'Authorization': 'Bearer $token'
    }, body: {
      "type": "profile",
      "name": name,
      "email": email,
      "nik": nik,
      "nohp": nohp,
      "alamat": alamat,
      "ktp": ktp,
      "id": id,
    });
    var res = json.decode(response.body);
    return res;
  }

  static Future detailUser(String id) async {
    var token = await SP.getToken();
    var endpoint = Uri.parse(url + "userDetail/$id");
    print("EAP : $endpoint");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    return res;
  }

  static Future getBars(String id) async {
    var kip = await SP.getOthers('kip');
    var token = await SP.getToken();
    var endpoint = Uri.parse(
        url + "FSVK08WMYB-JGLLYVOV8D-W97OEIWTCT/tes?kota=$id&kip=$kip");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    print("EP : $endpoint RES : $res");
    return res;
  }

  static Future simpulHome() async {
    var token = await SP.getToken();
    var endpoint = Uri.parse(url + "simpulHome");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    print("EP : $endpoint RES : $res");
    return res;
    // try {
    //   if (response.statusCode == 203) {
    //   } else {
    //     return 'Kombinasi username dan kata sandi anda tidak sesuai !';
    //   }
    // } on Exception catch (e) {
    //   return 'Error server : ' + e.toString();
    // }
  }

  static Future detailSimpul({required String type, program, location}) async {
    var token = await SP.getToken();
    var endpoint = Uri.parse(
        url + "detailSimpul?type=$type&program=$program&location=$location");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    print("EP : $endpoint RES : $res");
    return res;
  }

  static Future getBarsDetail(String id) async {
    var token = await SP.getToken();
    var kip = await SP.getOthers('kip');
    var endpoint = Uri.parse(
        url + "FSVK08WMYB-JGLLYVOV8D-W97OEIWTCT/tus?dapil=$id&kip=$kip");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    print("EP : $endpoint RES : $res");
    return {"res": res, "kip": kip};
  }

  static Future getBarsDetailByKec(String id) async {
    var token = await SP.getToken();
    var kip = await SP.getOthers('kip');
    var endpoint = Uri.parse(
        url + "FSVK08WMYB-JGLLYVOV8D-W97OEIWTCT/tis?kec=$id&kip=$kip");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    print("EP : $endpoint RES : $res");
    return {"res": res, "kip": kip};
  }
}
