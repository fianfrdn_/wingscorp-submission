import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kmf_app/core/endpoint.dart';
import 'package:kmf_app/vm/spf.dart';

class AnggotaVM {
  static String url = Server.ept;
  static Future detailUser(String id) async {
    var token = await SP.getToken();
    var endpoint = Uri.parse(url + "userDetail/$id");
    print("EAP : $endpoint");
    final response =
        await http.get(endpoint, headers: {'Authorization': 'Bearer $token'});
    var res = json.decode(response.body);
    return res;
  }
}
