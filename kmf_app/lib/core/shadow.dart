import 'package:flutter/material.dart';
import 'package:kmf_app/core/color.dart';

class Shadows {
  static final List<BoxShadow> thin = [
    BoxShadow(
        color: Colors.black.withOpacity(0.075),
        spreadRadius: 3,
        blurRadius: 10,
        offset: const Offset(0, 3))
  ];
  static final List<BoxShadow> normal = [
    const BoxShadow(
        color: Colors.black12,
        spreadRadius: 3,
        blurRadius: 10,
        offset: Offset(0, 3))
  ];
  static final List<BoxShadow> forbottom = [
    BoxShadow(
        color: Clr.primary.withOpacity(0.2),
        spreadRadius: 3,
        blurRadius: 10,
        offset: const Offset(0, 3))
  ];
}
