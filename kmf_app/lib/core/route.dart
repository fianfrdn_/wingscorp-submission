import 'package:flutter/material.dart';

class Routes {
  static stf(BuildContext context, StatefulWidget classes) {
    Navigator.push(context, MaterialPageRoute(builder: (_) {
      return classes;
    }));
  }

  static stl(BuildContext context, StatefulWidget classes) {
    Navigator.push(context, MaterialPageRoute(builder: (_) {
      return classes;
    }));
  }

  static stfA(BuildContext context, StatefulWidget classes) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
      return classes;
    }));
  }

  static stfR(BuildContext context, StatefulWidget classes) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
      return classes;
    }));
  }

  static stfX(BuildContext context, StatefulWidget classes) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) {
      return classes;
    }));
  }
}
