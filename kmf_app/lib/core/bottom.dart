// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/view/anggota/index.dart';
import 'package:kmf_app/view/home/index.dart';
import 'package:kmf_app/view/profile/index.dart';

class Navbar extends StatefulWidget {
  final int i;
  Navbar(this.i);
  @override
  NavbarState createState() => NavbarState();
}

class NavbarState extends State<Navbar> {
  final listPage = <StatefulWidget>[
    IndexHomeView(),
    IndexAnggotaView(),
    IndexProfileView(),
  ];
  void _navbarTapped(int index) {
    if (widget.i != index) {
      Routes.stfR(context, listPage[index]);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _bottomNavbarItem = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          icon: icGroup("asset/icon/bottom/1-${(widget.i + 1) == 1}.png"),
          label: "Dashboard"),
      BottomNavigationBarItem(
          icon: icGroup("asset/icon/bottom/2-${(widget.i + 1) == 2}.png"),
          label: "Anggota"),
      BottomNavigationBarItem(
          icon: icGroup("asset/icon/bottom/3-${(widget.i + 1) == 3}.png"),
          label: "Pengaturan"),
    ];
    return BottomNavigationBar(
      elevation: 2.75,
      type: BottomNavigationBarType.fixed,
      items: _bottomNavbarItem,
      backgroundColor: const Color(0xffffffff),
      currentIndex: widget.i,
      selectedItemColor: Color(0xffFF9900),
      selectedLabelStyle: GoogleFonts.poppins(
          color: Color(0xffFF9900),
          fontSize: 12,
          fontWeight: FontWeight.w600,
          letterSpacing: 0.5),
      unselectedLabelStyle:
          GoogleFonts.poppins(fontSize: 12.5, letterSpacing: 0.5),
      unselectedItemColor: Colors.black,
      onTap: _navbarTapped,
    );
  }

  Widget icGroup(String src) {
    return Container(
      height: 22,
      width: 22,
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          image: DecorationImage(fit: BoxFit.cover, image: AssetImage(src))),
    );
  }
}
