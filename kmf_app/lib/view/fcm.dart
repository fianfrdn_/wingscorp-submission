// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:kmf_app/core/route.dart';
// import 'package:kmf_app/view/auth/splash.dart';

// import '../../main.dart';

// class FcmBrigeStates extends StatefulWidget {
//   const FcmBrigeStates({Key? key}) : super(key: key);

//   @override
//   State<FcmBrigeStates> createState() => _FcmBrigeStatesState();
// }

// class _FcmBrigeStatesState extends State<FcmBrigeStates> {
//   @override
//   void initState() {
//     // FIREBASE FCM

//     // === KETIKA TERMINATED NOTIFICATION === //
//     FirebaseMessaging.instance.getInitialMessage().then((message) {
//       // print(message!.data.toString());
//       if (message == null) {
//         Routes.stfR(context, SplashAuthView());
//       } else {
//         // Navigator.push(context, MaterialPageRoute(builder: (_) {
//         //   return RouteFCM.standar(param: message.data);
//         // }));
//       }
//     });

//     FirebaseMessaging.onMessage.listen((RemoteMessage message) {
//       RemoteNotification notification = message.notification!;
//       AndroidNotification? android = message.notification?.android;
//       if (notification != null && android != null && !kIsWeb) {
//         flutterLocalNotificationsPlugin!.show(
//             notification.hashCode,
//             notification.title,
//             notification.body,
//             NotificationDetails(
//                 android: AndroidNotificationDetails(channel!.id, channel!.name,
//                     icon: 'launch_background')));
//       }
//     });

//     // === KETIKA ON BACKGROUND NOTIFICATION === //
//     FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//       if (message == null) {
//         Routes.stfR(context, SplashAuthView());
//       } else {
//         // Navigator.push(context, MaterialPageRoute(builder: (_) {
//         // return RouteFCM.standar(param: message.data);
//         // }));
//       }
//     });
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         theme: ThemeData(
//             primarySwatch: Colors.blue, canvasColor: Colors.transparent),
//         debugShowCheckedModeBanner: false,
//         home: SplashAuthView());
//   }
// }
