// ignore_for_file: prefer_const_constructors, unnecessary_string_interpolations

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/edit.dart';
import 'package:kmf_app/view/anggota/index.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/vm/anggota.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class DetailAnggotaView extends StatefulWidget {
  final String id;
  final bool current;

  DetailAnggotaView({Key? key, required this.id, this.current = false})
      : super(key: key);

  @override
  State<DetailAnggotaView> createState() => _DetailAnggotaViewState();
}

class _DetailAnggotaViewState extends State<DetailAnggotaView> {
  Map data = {};
  TextEditingController reject = TextEditingController();
  bool loading = false;
  bool loadingData = true;
  int roleLevel = 0;
  String roleString = 'korcab';
  String currentRoles = '';
  List<String> roleListString = ['admin', 'korcab', 'korpil', 'korcam', 'user'];
  getCurrentRole() async {
    String tmpRole = await SP.getOthers('role');
    setState(() {
      roleLevel =
          (roleListString.indexWhere((element) => element == tmpRole)) + 1;
      currentRoles = tmpRole;
    });
    print("roleLevel : $roleLevel");
  }

  getData() {
    AnggotaVM.detailUser(widget.id).then((value) {
      setState(() {
        data = value;
        loadingData = false;
      });
    });
  }

  String accVerif = '1';
  @override
  void initState() {
    getCurrentRole();
    getData();
    super.initState();
  }

  routePage(BuildContext context) async {
    var stepPage =
        await Navigator.push(context, MaterialPageRoute(builder: (_) {
      return EditAnggotaView(data: data);
    }));
    try {
      final DetailAnggotaView args = stepPage;
      print("object $args");
    } catch (e) {}
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 0,
        elevation: 1.25,
        shadowColor: Colors.black54,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: displayWidth(context) * 0.075,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('asset/icon/back.png'))),
              ),
            ),
            SizedBox(width: displayWidth(context) * 0.05),
            Text(
              "Detail Pengguna",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
        actions: [
          TextButton(
              onPressed: () {
                // Routes.stf(context, EditAnggotaView(data: data));
                routePage(context);
              },
              child: Text(
                "Edit",
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 16,
                    letterSpacing: 0.22,
                    fontWeight: FontWeight.w500),
              ))
        ],
      ),
      body: Container(
        color: Color(0xffF5F5F5),
        width: displayWidth(context),
        height: displayHeight(context),
        child: ListView(
          children: [
            // SizedBox(height: 25),
            if (!loadingData) ...[
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                width: displayWidth(context),
                child: Column(
                  children: [
                    Text("Data Diri Pengguna",
                        style: stl3, textAlign: TextAlign.left),
                    SizedBox(height: 10),
                    lr("Nama", data["name"]),
                    lr("NIK", data["nik"]),
                    lr("Nohp", data["nohp"]),
                    lr("Email", data["email"]),
                    lr("UserID", data["id"].toString()),
                    lr("Tgl. Daftar", data["created_at"]),
                  ],
                ),
              ),
              SizedBox(height: 8.5),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                color: Colors.white,
                width: displayWidth(context),
                child: Column(
                  children: [
                    Text("Detail Pengguna",
                        style: stl3, textAlign: TextAlign.left),
                    SizedBox(height: 10),
                    lr("Kota",
                        data["kota"] == null ? "-" : data["kota"]["name"]),
                    lr("Kecamatan",
                        data["kec"] == null ? "-" : data["kec"]["name"]),
                    lr(
                        "Dapil",
                        data["kec"] == null
                            ? "-"
                            : data["kec"]["dapil"]["name"]),
                    lr("Desa",
                        data["desa"] == null ? "-" : data["desa"]["name"]),
                    lr(
                        "Program",
                        data["program"] == null
                            ? "-"
                            : data["program"]["name"]),
                    lr("Alamat", data["alamat"]),
                  ],
                ),
              ),
              SizedBox(height: 8.5),
              if ((roleLevel > 3) == false && !widget.current) ...[
                if (data["status"].toString() == "0") ...[
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    color: Colors.white,
                    width: displayWidth(context),
                    child: Container(
                      width: displayWidth(context),
                      margin: EdgeInsets.symmetric(
                          horizontal: displayWidth(context) * 0.04),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Center(
                              child: Text("Pilih Role Pengguna",
                                  style: stl3, textAlign: TextAlign.center)),
                          SizedBox(height: 15),
                          SizedBox(
                              width: displayWidth(context) * 0.6,
                              child: Column(
                                children: [
                                  if (roleLevel == 0) ...[
                                    radio("Admin Sistem", "admin"),
                                    radio("Koordinator Cabang", "korcab"),
                                    radio("Koordinator Dapil", "korpil"),
                                    radio("Koordinator Kecamatan", "korcam"),
                                    radio("Pengguna", "user"),
                                  ] else if (roleLevel == 1) ...[
                                    radio("Koordinator Cabang", "korcab"),
                                    radio("Koordinator Dapil", "korpil"),
                                    radio("Koordinator Kecamatan", "korcam"),
                                    radio("Pengguna", "user"),
                                  ] else if (roleLevel == 2) ...[
                                    radio("Koordinator Dapil", "korpil"),
                                    radio("Koordinator Kecamatan", "korcam"),
                                    radio("Pengguna", "user"),
                                  ] else if (roleLevel == 3) ...[
                                    radio("Koordinator Kecamatan", "korcam"),
                                    radio("Pengguna", "user"),
                                  ] else if (roleLevel == 4) ...[
                                    radio("Pengguna", "user"),
                                  ],
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                ]
              ],
              SizedBox(height: 8.5),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                color: Colors.white,
                width: displayWidth(context),
                child: Column(
                  children: [
                    Text("Kartu Identitas Pengguna",
                        style: stl3, textAlign: TextAlign.left),
                    SizedBox(height: 10),
                    Container(
                      height: 200,
                      width: displayWidth(context),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.fitHeight,
                              image: NetworkImage(data["ktp"]))),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8.5),
              if (!widget.current) ...[
                if ((roleLevel > 3) == false && !widget.current) ...[
                  if (data["status"].toString() == "0") ...[
                    Container(
                      color: Colors.white,
                      // height: 120,
                      padding: EdgeInsets.symmetric(
                          horizontal: displayWidth(context) * 0.03,
                          vertical: 10),
                      width: displayWidth(context),
                      margin: EdgeInsets.symmetric(
                          horizontal: displayWidth(context) * 0.01),
                      child: Column(
                        children: [
                          Text("Verifikasi Pengguna",
                              style: stl3, textAlign: TextAlign.left),
                          SizedBox(height: 17.5),
                          radioVerif("Terima Pengguna", "1"),
                          radioVerif("Tolak Pengguna", "2"),
                          if (accVerif == "2") ...[
                            SizedBox(height: 17.5),
                            InputWidget(
                                maxLines: 4,
                                enabled: true,
                                controller: reject,
                                label: 'Alasan penolakan',
                                useSuffix: false,
                                usePreffix: false,
                                icon: ''),
                          ],
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                    SizedBox(height: 17.5),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: displayWidth(context) * 0.03),
                      child: Btn(
                          radius: 6.5,
                          function: () {},
                          clr: Clr.primaryButton,
                          outline: Clr.primaryButton,
                          elevated: 0.1,
                          // ignore: sort_child_properties_last
                          child: Text(loading ? "Loading..." : "Verifikasi",
                              style: stl3),
                          width: displayWidth(context)),
                    ),
                  ]
                ]
              ],
            ] else
              ...[],
            SizedBox(height: 37.5),
          ],
        ),
      ),
    );
  }

  Widget radio(String val, role) {
    bool active = roleString == role;
    return InkWell(
      onTap: () {
        setState(() {
          roleString = role;
        });
        print("roleString : $roleString");
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 4),
        child: Row(
          children: [
            Icon(active ? Icons.circle : Icons.circle_outlined,
                size: 18, color: active ? Clr.primaryButton : Colors.black),
            SizedBox(width: 5),
            Text(
              val,
              style: active ? stl2 : stl1,
            ),
          ],
        ),
      ),
    );
  }

  Widget radioVerif(String val, role) {
    bool active = accVerif == role ? true : false;
    return InkWell(
      onTap: () {
        setState(() {
          accVerif = role;
        });
        print("roleString : $accVerif");
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 4),
        child: Row(
          children: [
            Icon(active ? Icons.circle : Icons.circle_outlined,
                size: 18, color: active ? Clr.primaryButton : Colors.black),
            SizedBox(width: 5),
            Text(
              val,
              style: active ? stl2 : stl1,
            ),
          ],
        ),
      ),
    );
  }

  Widget lr(String l, r) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.04),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              width: displayWidth(context) * 0.25,
              child: Text("$l", style: stl1)),
          SizedBox(
              width: displayWidth(context) * 0.035,
              child: Text(":", style: stl1)),
          SizedBox(
              width: displayWidth(context) * 0.6,
              child: Text("$r", style: stl2))
        ],
      ),
    );
  }

  TextStyle stl1 = GoogleFonts.poppins(
      color: Color(0xff000000),
      fontSize: 13,
      letterSpacing: 0.25,
      fontWeight: FontWeight.w500);
  TextStyle stl2 = GoogleFonts.poppins(
      color: Color(0xff000000),
      fontSize: 13,
      letterSpacing: 0.25,
      fontWeight: FontWeight.w600);
  TextStyle stl3 = GoogleFonts.poppins(
      color: Color(0xff000000),
      fontSize: 15,
      letterSpacing: 0.25,
      fontWeight: FontWeight.w600);
}
