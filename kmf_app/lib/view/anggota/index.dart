import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/bottom.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/create.dart';
import 'package:kmf_app/view/anggota/detail.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/view/widget/modal/standart.dart';
import 'package:kmf_app/view/widget/reject.dart';
import 'package:kmf_app/view/widget/waitwidget.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class IndexAnggotaView extends StatefulWidget {
  final String idUser;
  const IndexAnggotaView({Key? key, this.idUser = ''}) : super(key: key);

  @override
  State<IndexAnggotaView> createState() => _IndexAnggotaViewState();
}

class _IndexAnggotaViewState extends State<IndexAnggotaView> {
  TextEditingController search = TextEditingController();
  Map data = {};
  String tab = 'wait';
  bool loading = true;
  getData({String q = ''}) {}

  bool loadingTagUser = true;
  String role = '';
  String status = '';
  String rejects = '';
  Future getCurrentTMP() async {
    String tmp = await SP.getOthers("role");
    String statusD = await SP.getOthers("status");
    String rejectsD = await SP.getOthers("rejects") ?? "-";
    setState(() {
      role = tmp;
      status = statusD;
      rejects = rejectsD;
      loadingTagUser = false;
      // if (tmp == "korcam") {
      // }
    });
  }

  @override
  void initState() {
    getCurrentTMP().then((value) {
      if (status == "1") {
        getData();
      }
    });
    super.initState();
  }

  routePage(String id, BuildContext context) async {
    var stepPage =
        await Navigator.push(context, MaterialPageRoute(builder: (_) {
      return DetailAnggotaView(id: id);
    }));
    try {
      final IndexAnggotaView? args = stepPage;
      if (args?.idUser != null) {
        setState(() {
          data["data"].removeWhere(
              (y) => y["id"].toString() == args?.idUser.toString());
        });
        modal("Alert", "Berhasil memverifikasi anggota");
      }
      print("object ${args?.idUser}");
    } catch (e) {}
  }

  void modal(String title, val) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ModalStdLog(title, val));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: Navbar(1),
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.only(
                top: 5,
                right: displayWidth(context) * 0.05,
                left: displayWidth(context) * 0.01),
            child: Text(
              "Hai, $role 👋",
              textAlign: TextAlign.start,
              style: GoogleFonts.poppins(
                  color: Color(0xff4D4D4D),
                  fontSize: 18,
                  letterSpacing: 0.25,
                  fontWeight: FontWeight.w700),
            ),
          ),
          leadingWidth: displayWidth(context) * 0,
          elevation: 0,
          backgroundColor: Colors.white),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        // padding: EdgeInsets.zero,
        padding: EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.05),
        child: Column(
          mainAxisAlignment: status == "1"
              ? MainAxisAlignment.start
              : MainAxisAlignment.center,
          children: [
            if (status == "1") ...[
              SizedBox(height: displayHeight(context) * 0.025),
              InkWell(
                onTap: () {
                  Routes.stf(context, CreateAnggotaView());
                },
                child: Row(
                  children: [
                    Container(
                        child: Icon(Icons.person_add, color: Color(0xffFF9900)),
                        padding: EdgeInsets.all(12.5),
                        decoration: BoxDecoration(
                            color: Color(0xffFFE4BC),
                            borderRadius:
                                BorderRadius.all(Radius.circular(9.5)))),
                    SizedBox(width: 12.5),
                    Text(
                      "Tambah Anggota",
                      textAlign: TextAlign.start,
                      style: GoogleFonts.nunitoSans(
                          color: Color(0xff4D4D4D),
                          fontSize: 16,
                          letterSpacing: 0.15,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              SizedBox(height: displayHeight(context) * 0.02),
              Container(
                width: displayWidth(context),
                child: Row(
                  children: [
                    Text(
                      "Total Anggota Ditambahkan : ",
                      textAlign: TextAlign.start,
                      style: GoogleFonts.poppins(
                          color: Color(0xff000000),
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      loading ? " memuat" : " ${data['total']}",
                      textAlign: TextAlign.start,
                      style: GoogleFonts.poppins(
                          color: Color(0xff000000),
                          fontSize: 15,
                          letterSpacing: 0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(height: displayHeight(context) * 0.01),
              Row(
                children: [
                  btnDS("Belum Verif", tab == "wait" ? true : false, "wait"),
                  SizedBox(width: 5),
                  btnDS("Verif", tab == "done" ? true : false, "done"),
                  SizedBox(width: 5),
                  btnDS("Ditolak", tab == "reject" ? true : false, "reject"),
                ],
              ),
              SizedBox(height: displayHeight(context) * 0.02),
              Container(
                height: 40,
                width: displayWidth(context),
                child: Stack(
                  children: [
                    InputWidget(
                        controller: search,
                        label: 'Cari Anggota',
                        icon: 'asset/icon/charm_search.png'),
                    Positioned(
                        right: 5,
                        top: 6,
                        child: Btn(
                            child: Text(
                              "Cari",
                              textAlign: TextAlign.start,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  letterSpacing: 0.25,
                                  fontWeight: FontWeight.w500),
                            ),
                            radius: 4,
                            height: 29,
                            elevated: 0,
                            function: () {
                              getData(q: "s=${search.text}");
                            },
                            clr: Color(0xffFF9900),
                            outline: Color(0xffFF9900),
                            width: displayWidth(context) * 0.175))
                  ],
                ),
              ),
              // getData(q: "s=${search.text}");
              SizedBox(height: displayHeight(context) * 0.02),
              Container(
                width: displayWidth(context),
                height: displayHeight(context) * 0.52,
                // color: Colors.red,
                child: ListView(
                  children: [
                    if (loading) ...[
                      Text(
                        "Memuat data",
                        textAlign: TextAlign.start,
                        style: GoogleFonts.poppins(
                            color: Color(0xff4D4D4D),
                            fontSize: 15,
                            letterSpacing: 0.25,
                            fontWeight: FontWeight.w700),
                      ),
                    ] else ...[
                      if (data["data"].length == 0) ...[
                        Text(
                          "Belum ada pengguna",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                              color: Color(0xff4D4D4D),
                              fontSize: 15,
                              letterSpacing: 0.25,
                              fontWeight: FontWeight.w700),
                        ),
                      ] else ...[
                        for (var a = 0; a < data["data"].length; a++) ...[
                          InkWell(
                            onTap: () {
                              routePage(
                                  data["data"][a]["id"].toString(), context);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10,
                                  horizontal: displayWidth(context) * 0.025),
                              margin: EdgeInsets.only(bottom: 5),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4.5)),
                                  border: Border.all(
                                      color: Color(0xffEDEDED), width: 1)),
                              child: Column(
                                children: [
                                  lr("Nama", data["data"][a]["name"]),
                                  lr("NIK", data["data"][a]["nik"]),
                                  lr("Lokasi",
                                      "Dapil ${data["data"][a]["kec"]["dapil"]["name"]}"),
                                ],
                              ),
                            ),
                          ),
                        ]
                      ]
                    ]
                  ],
                ),
              )
            ] else if (status == "0") ...[
              WaitWidget()
            ] else if (status == "2") ...[
              RejectWidget(val: rejects)
            ]
          ],
        ),
      ),
    );
  }

  Widget lr(l, r) {
    return Row(
      children: [
        SizedBox(
          width: displayWidth(context) * 0.2,
          child: Text(
            "$l",
            textAlign: TextAlign.start,
            style: GoogleFonts.poppins(
                color: Color(0xff000000),
                fontSize: 13,
                letterSpacing: 0.25,
                fontWeight: FontWeight.w400),
          ),
        ),
        SizedBox(
          width: displayWidth(context) * 0.025,
          child: Text(
            ":",
            textAlign: TextAlign.start,
            style: GoogleFonts.poppins(
                color: Color(0xff000000),
                fontSize: 13,
                letterSpacing: 0.25,
                fontWeight: FontWeight.w500),
          ),
        ),
        Text(
          "$r",
          textAlign: TextAlign.start,
          style: GoogleFonts.poppins(
              color: Color(0xff000000),
              fontSize: 13,
              letterSpacing: 0.25,
              fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  Widget btnDS(String val, bool active, String route) {
    return Btn(
        child: Text(
          val,
          textAlign: TextAlign.start,
          style: GoogleFonts.poppins(
              color: active ? Colors.white : Color(0xffFFB800),
              fontSize: 12.5,
              letterSpacing: 0,
              fontWeight: FontWeight.w600),
        ),
        height: 35,
        elevated: 0,
        radius: 7.5,
        function: () {
          setState(() {
            search.text = "";
            tab = route;
          });
          getData();
        },
        clr: active ? Color(0xffFFB800) : Colors.white,
        outline: Color(0xffFFB800),
        width: displayWidth(context) * (val != 'Verif' ? 0.275 : 0.185));
  }
}
