import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/index.dart';
import 'package:kmf_app/view/auth/nikcheck.dart';
import 'package:kmf_app/view/auth/widget/modbo.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/view/widget/modal/modalloop.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class CreateAnggotaView extends StatefulWidget {
  final String select;
  const CreateAnggotaView({Key? key, this.select = ''}) : super(key: key);

  @override
  State<CreateAnggotaView> createState() => _CreateAnggotaViewState();
}

class _CreateAnggotaViewState extends State<CreateAnggotaView> {
  int roleLevel = 0;
  List<String> roleListString = ['admin', 'korcab', 'korpil', 'korcam', 'user'];
  bool obsecureText1 = true;
  bool obsecureText2 = true;
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController c_password = TextEditingController();
  TextEditingController nik = TextEditingController();
  TextEditingController nohp = TextEditingController();
  TextEditingController ktp = TextEditingController();
  TextEditingController alamat = TextEditingController();
  TextEditingController kota_id = TextEditingController();
  TextEditingController kecamatan_id = TextEditingController();
  TextEditingController desa_id = TextEditingController();
  TextEditingController program_id = TextEditingController();
  TextEditingController program_lainnya = TextEditingController();
  TextEditingController role_id = TextEditingController();

  bool selectKota = false;
  bool selectKec = false;
  bool selectDesa = false;
  bool selectProgram = false;
  bool selectRole = false;

  List dataKota = [];
  List dataKec = [];
  List dataDesa = [];
  Map dataKotaMap = {};
  Map dataKecMap = {};
  Map dataDesaMap = {};
  Map dataProgramMap = {};
  Map dataRoleMap = {};
  bool loadingKec = true;
  File? imageFile;

  void _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxHeight: 1080,
      maxWidth: 1080,
    );
    setState(() {
      imageFile = File(pickedFile!.path);
      var a = imageFile.toString().split("/");
      // print();
      ktp.text = a[a.length - 1];
    });
  }

  void _showErrorDialog(String title, Map val) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ModalLoop(title, val));
  }

  bool loadingStore = false;
  storeFunction() async {}

  Future loadKota({String type = ''}) async {
    String jsonString =
        await rootBundle.loadString("asset/json/location/kota.json");
    final jsonData = json.decode(jsonString);
    setState(() {
      dataKota = jsonData["kota"];
      if (selectKota) {
        kota_id.text = jsonData["kota"]["name"].toString();
      }
    });
    print(jsonData.toString());
  }

  Future loadKec(String id) async {
    String jsonString =
        await rootBundle.loadString("asset/json/location/kec.json");
    final jsonData = json.decode(jsonString);
    setState(() {
      loadingKec = false;
      dataKec = jsonData['kecamatan'][id];
      if (selectKec) {
        kecamatan_id.text = jsonData["kecamatan"]["name"].toString();
      }
    });
    print(jsonData['kecamatan'][id].toString());
  }

  Future loadDesa(String id) async {
    String jsonString =
        await rootBundle.loadString("asset/json/location/desa.json");
    final jsonData = json.decode(jsonString);
    setState(() {
      loadingKec = false;
      dataDesa = jsonData[id];
      if (selectDesa) {
        desa_id.text = jsonData["name"].toString();
      }
    });
    print(jsonData[id].toString());
  }

  List program = [];
  loadAPI() {}

  List role = [
    {"id": "admin", "name": "admin"},
    {"id": "korcab", "name": "korcab"},
    {"id": "korpil", "name": "korpil"},
    {"id": "korcam", "name": "korcam"},
    {"id": "user", "name": "user"}
  ];

  getCurrentRole() async {
    String tmpRole = await SP.getOthers('role');
    String tmpKotaId = await SP.getOthers('kota_id');
    String tmpKecamatanId = await SP.getOthers('kecamatan_id');
    String tmpProgramId = await SP.getOthers('program_id');
    String jsonKota =
        await rootBundle.loadString("asset/json/location/kota.json");
    final jsonDataKota = json.decode(jsonKota);
    int indexKota = jsonDataKota["kota"]
        .indexWhere((element) => element["id"].toString() == tmpKotaId);

    String jsonKec =
        await rootBundle.loadString("asset/json/location/kec.json");
    final jsonDataKec = json.decode(jsonKec);

    if (tmpRole == "korpil") {
      var dapilGrouped = jsonDataKec["kecamatan"][tmpKotaId]
          .where((elekmen) => elekmen["dapil_id"] == int.parse(tmpProgramId));
      dapilGrouped.forEach((item) {
        setState(() {
          dataKec.add(item);
        });
      });
    }

    int indexKec = jsonDataKec["kecamatan"][tmpKotaId]
        .indexWhere((element) => element["id"].toString() == tmpKecamatanId);

    String jsonDesa =
        await rootBundle.loadString("asset/json/location/desa.json");
    final jsonDataDesa = json.decode(jsonDesa);
    print("Desa : ${jsonDataDesa[tmpKecamatanId]}");

    // INI JIKA KORCAM
    setState(() {
      kota_id.text = jsonDataKota["kota"][indexKota]["name"];
      kecamatan_id.text = jsonDataKec["kecamatan"][tmpKotaId][indexKec]["name"];
      dataKotaMap = jsonDataKota["kota"][indexKota];
      dataKecMap = jsonDataKec["kecamatan"][tmpKotaId][indexKec];
      dataDesa = jsonDataDesa[tmpKecamatanId];
      roleLevel =
          (roleListString.indexWhere((element) => element == tmpRole)) + 1;
    });
  }

  @override
  void initState() {
    getCurrentRole();
    loadKota();
    loadAPI();
    // loadKec('3508');
    super.initState();
  }

  openModal({String type = "kota"}) async {
    List tempDatas = [];
    if (type == "kota") {
      tempDatas = dataKota;
    } else if (type == "kec") {
      tempDatas = dataKec;
    } else if (type == "desa") {
      tempDatas = dataDesa;
    } else if (type == "program") {
      tempDatas = program;
    } else if (type == "role") {
      List customArray = [];
      for (var h = roleLevel; h < role.length; h++) {
        customArray.add(role[h]);
      }
      tempDatas = customArray;
    }
    var modhal = await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext buildContext) {
          return ModalBottomAuths(data: tempDatas, type: "onCreateAnggota");
        });
    try {
      final CreateAnggotaView args = modhal;
      var indx =
          tempDatas.indexWhere((fl) => fl["id"].toString() == args.select);
      print(tempDatas[indx].toString());
      if (type == "kota") {
        dataKotaMap = tempDatas[indx];
        loadKec(dataKotaMap["id"].toString());
        setState(() {
          kota_id.text = dataKotaMap["name"];
          selectKota = true;
        });
      } else if (type == "kec") {
        dataKecMap = tempDatas[indx];
        loadDesa(dataKecMap["id"].toString());
        setState(() {
          kecamatan_id.text = dataKecMap["name"];
          selectKec = true;
        });
      } else if (type == "desa") {
        dataDesaMap = tempDatas[indx];
        setState(() {
          selectDesa = true;
          desa_id.text = dataDesaMap["name"];
        });
      } else if (type == "program") {
        dataProgramMap = tempDatas[indx];
        setState(() {
          selectProgram = true;
          program_id.text = dataProgramMap["name"].toString();
        });
      } else if (type == "role") {
        dataRoleMap = tempDatas[indx];
        setState(() {
          selectRole = true;
          role_id.text = dataRoleMap["name"].toString();
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF5F5F5),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 0,
        elevation: 1.25,
        shadowColor: Colors.black54,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: displayWidth(context) * 0.075,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('asset/icon/back.png'))),
              ),
            ),
            SizedBox(width: displayWidth(context) * 0.05),
            Text(
              "Tambah Anggota",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
      body: Container(
          height: displayHeight(context),
          width: displayWidth(context),
          padding: EdgeInsets.zero,
          child: Padding(
            padding: EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 35),
                  padding: EdgeInsets.only(
                      top: 20,
                      bottom: 30,
                      left: displayWidth(context) * 0.04,
                      right: displayWidth(context) * 0.04),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Column(
                    children: [
                      InputWidget(
                          controller: name,
                          label: 'Nama',
                          icon: 'asset/icon/auth/nik.png'),
                      SizedBox(height: 17.5),
                      Stack(
                        children: [
                          InputWidget(
                              type: TextInputType.number,
                              controller: nik,
                              label: 'NIK',
                              icon: 'asset/icon/auth/nik.png'),
                          Positioned(
                              top: 8.5,
                              right: 10,
                              child: InkWell(
                                  onTap: () {
                                    // print("HA");
                                    Routes.stf(context,
                                        NikCheckAuthView(nik: nik.text));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Clr.primaryButton,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    height: 30,
                                    alignment: Alignment.center,
                                    width: displayWidth(context) * 0.185,
                                    child: Text("Cek NIK",
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.nunito(
                                            color: Colors.black,
                                            fontSize: 13,
                                            letterSpacing: 0.25,
                                            fontWeight: FontWeight.w500)),
                                  )))
                        ],
                      ),
                      SizedBox(height: 17.5),
                      InputWidget(
                          controller: email,
                          label: 'Email',
                          type: TextInputType.emailAddress,
                          icon: 'asset/icon/auth/nik.png'),
                      SizedBox(height: 17.5),
                      InputWidget(
                          controller: nohp,
                          type: TextInputType.phone,
                          label: 'No Hp',
                          icon: 'asset/icon/auth/nohp.png'),
                      SizedBox(height: 17.5),
                      InkWell(
                        onTap: () {
                          openModal(type: "role");
                        },
                        child: InputWidget(
                            enabled: false,
                            controller: role_id,
                            useSuffix: true,
                            suffixIcon: Icons.keyboard_arrow_down,
                            label: 'Role',
                            icon: 'asset/icon/auth/kota.png'),
                      ),
                      SizedBox(height: 17.5),
                      InkWell(
                        onTap: () {
                          openModal(type: "kota");
                        },
                        child: InputWidget(
                            enabled: false,
                            controller: kota_id,
                            useSuffix: true,
                            suffixIcon: Icons.keyboard_arrow_down,
                            label: 'Kota',
                            icon: 'asset/icon/auth/kota.png'),
                      ),
                      SizedBox(height: 17.5),
                      InkWell(
                        onTap: () {
                          openModal(type: "kec");
                        },
                        child: InputWidget(
                            enabled: false,
                            useSuffix: true,
                            suffixIcon: Icons.keyboard_arrow_down,
                            controller: kecamatan_id,
                            label: 'Kecamatan',
                            icon: 'asset/icon/auth/kecamatan.png'),
                      ),
                      SizedBox(height: 17.5),
                      InkWell(
                        onTap: () {
                          openModal(type: "desa");
                        },
                        child: InputWidget(
                            enabled: false,
                            controller: desa_id,
                            useSuffix: true,
                            suffixIcon: Icons.keyboard_arrow_down,
                            label: 'Desa',
                            icon: 'asset/icon/auth/desa.png'),
                      ),
                      SizedBox(height: 17.5),
                      InputWidget(
                          controller: alamat,
                          label: 'Alamat',
                          icon: 'asset/icon/auth/alamat.png'),
                      // FOTO KTP
                      SizedBox(height: 17.5),
                      InkWell(
                        onTap: () {
                          _getFromGallery();
                        },
                        child: InputWidget(
                            enabled: false,
                            controller: ktp,
                            label: 'Foto KTP',
                            icon: 'asset/icon/auth/nik.png'),
                      ),
                      if (imageFile != null) ...[
                        SizedBox(height: 10),
                        Stack(
                          children: [
                            Image.file(imageFile!),
                            Positioned(
                                top: 10,
                                right: 10,
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      ktp.text = '';
                                      imageFile = null;
                                    });
                                  },
                                  icon: Icon(Icons.cancel, size: 28),
                                ))
                          ],
                        )
                      ],
                      // PROGRAM
                      SizedBox(height: 17.5),
                      InputWidgetFun(
                          fun: () {
                            setState(() {
                              obsecureText1 = !obsecureText1;
                            });
                          },
                          obsecureText: obsecureText1,
                          useSuffix: true,
                          suffixIcon: Icon(Icons.remove_red_eye),
                          controller: password,
                          label: 'Password',
                          icon: 'asset/icon/auth/password.png'),

                      SizedBox(height: 17.5),

                      InputWidgetFun(
                          fun: () {
                            setState(() {
                              obsecureText2 = !obsecureText2;
                            });
                          },
                          obsecureText: obsecureText2,
                          useSuffix: true,
                          suffixIcon: Icon(Icons.remove_red_eye),
                          controller: c_password,
                          label: 'Konfirmasi Password',
                          icon: 'asset/icon/auth/password.png'),
                      SizedBox(height: 17.5),
                      SizedBox(
                        width: displayWidth(context),
                        child: Text("Anda mengenal bang pur darimana ?",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.nunito(
                                color: Colors.black,
                                fontSize: 13.5,
                                letterSpacing: 0.25,
                                fontWeight: FontWeight.w400)),
                      ),
                      // PROGRAM KENAL
                      SizedBox(height: 12.5),
                      InkWell(
                        onTap: () {
                          openModal(type: "program");
                        },
                        child: InputWidget(
                            enabled: false,
                            controller: program_id,
                            label: 'Program',
                            useSuffix: true,
                            usePreffix: false,
                            suffixIcon: Icons.keyboard_arrow_down,
                            icon: ''),
                      ),
                      if (program_id.text
                          .toLowerCase()
                          .contains('lainnya')) ...[
                        SizedBox(height: 17.5),
                        InputWidget(
                            maxLines: 6,
                            enabled: true,
                            controller: program_lainnya,
                            label: 'Program lainnya ...',
                            useSuffix: false,
                            usePreffix: false,
                            icon: ''),
                      ],
                      // LAINNYA KENAL
                      SizedBox(height: 20),
                      Btn(
                          child: Text(
                            "Tambahkan Anggota",
                            style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          radius: 10,
                          function: () {
                            storeFunction();
                          },
                          clr: Clr.primaryButton,
                          outline: Clr.primaryButton,
                          elevated: 1.5,
                          width: displayWidth(context)),
                    ],
                  ),
                ),
                SizedBox(width: 30),
              ],
            ),
          )),
    );
  }
}
