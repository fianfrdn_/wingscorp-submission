import 'dart:convert';
import "dart:io";
import 'dart:typed_data';

import "package:flutter/material.dart";
import "package:flutter/src/foundation/key.dart";
import "package:flutter/src/widgets/framework.dart";
import "package:google_fonts/google_fonts.dart";
import "package:image_picker/image_picker.dart";
import "package:kmf_app/core/color.dart";
import "package:kmf_app/core/route.dart";
import "package:kmf_app/core/size.dart";
import 'package:kmf_app/view/anggota/detail.dart';
import "package:kmf_app/view/auth/nikcheck.dart";
import "package:kmf_app/view/widget/button.dart";
import "package:kmf_app/view/widget/inputs.dart";
import "package:kmf_app/vm/anggota.dart";

class EditAnggotaView extends StatefulWidget {
  final Map data;
  const EditAnggotaView({Key? key, required this.data}) : super(key: key);

  @override
  State<EditAnggotaView> createState() => _EditAnggotaViewState();
}

class _EditAnggotaViewState extends State<EditAnggotaView> {
  File? imageFile;
  TextEditingController ktp = TextEditingController();

  TextEditingController name = TextEditingController();
  TextEditingController nik = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController nohp = TextEditingController();
  TextEditingController alamat = TextEditingController();

  update() async {
    Uint8List imageRaw = await imageFile!.readAsBytes();
    ktp.text = base64.encode(imageRaw);
    AnggotaVM.updateUser(
      id: widget.data["id"].toString(),
      ktp: ktp.text.toString(),
      name: name.text.toString(),
      nik: nik.text.toString(),
      email: email.text.toString(),
      nohp: nohp.text.toString(),
      alamat: alamat.text.toString(),
    ).then((value) {
      Navigator.pop(
          context, DetailAnggotaView(id: widget.data["id"].toString()));
    });
  }

  void _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxHeight: 1080,
      maxWidth: 1080,
    );
    setState(() {
      imageFile = File(pickedFile!.path);
      var a = imageFile.toString().split("/");
      // print();
      ktp.text = a[a.length - 1];
    });
  }

  @override
  void initState() {
    setState(() {
      name.text = widget.data['name'];
      nik.text = widget.data['nik'];
      email.text = widget.data['email'];
      nohp.text = widget.data['nohp'];
      alamat.text = widget.data['alamat'];
      ktp.text = "X";
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF5F5F5),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 0,
        elevation: 1.25,
        shadowColor: Colors.black54,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: displayWidth(context) * 0.075,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("asset/icon/back.png"))),
              ),
            ),
            SizedBox(width: displayWidth(context) * 0.05),
            Text(
              "Edit Data Anggota",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
      body: Container(
          height: displayHeight(context),
          width: displayWidth(context),
          padding: EdgeInsets.zero,
          child: Container(
              color: Colors.white,
              margin: EdgeInsets.fromLTRB(displayWidth(context) * 0.025, 20,
                  displayWidth(context) * 0.025, 0),
              padding: EdgeInsets.symmetric(
                  vertical: 14, horizontal: displayWidth(context) * 0.04),
              child: ListView(children: [
                InputWidget(
                    controller: name,
                    label: "Nama",
                    icon: "asset/icon/auth/nik.png"),
                SizedBox(height: 17.5),
                Stack(
                  children: [
                    InputWidget(
                        controller: nik,
                        label: "NIK",
                        icon: "asset/icon/auth/nik.png"),
                    Positioned(
                        top: 8.5,
                        right: 10,
                        child: InkWell(
                            onTap: () {
                              // print("HA");
                              Routes.stf(
                                  context, NikCheckAuthView(nik: nik.text));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Clr.primaryButton,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              height: 30,
                              alignment: Alignment.center,
                              width: displayWidth(context) * 0.185,
                              child: Text("Cek NIK",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.nunito(
                                      color: Colors.black,
                                      fontSize: 13,
                                      letterSpacing: 0.25,
                                      fontWeight: FontWeight.w500)),
                            )))
                  ],
                ),
                SizedBox(height: 17.5),
                InputWidget(
                    controller: email,
                    label: "Email",
                    icon: "asset/icon/auth/nik.png"),
                SizedBox(height: 17.5),
                InputWidget(
                    controller: nohp,
                    label: "No Hp",
                    icon: "asset/icon/auth/nohp.png"),
                SizedBox(height: 17.5),
                InputWidget(
                    controller: alamat,
                    label: "Alamat",
                    icon: "asset/icon/auth/alamat.png"),
                SizedBox(height: 17.5),
                InkWell(
                  onTap: () {
                    _getFromGallery();
                  },
                  child: InputWidget(
                      enabled: false,
                      controller: ktp,
                      label: "Foto KTP",
                      icon: "asset/icon/auth/nik.png"),
                ),
                if (imageFile != null) ...[
                  SizedBox(height: 10),
                  Stack(
                    children: [
                      Image.file(imageFile!),
                      Positioned(
                          top: 10,
                          right: 10,
                          child: IconButton(
                            onPressed: () {
                              setState(() {
                                ktp.text = "X";
                                imageFile = null;
                              });
                            },
                            icon: Icon(Icons.cancel, size: 28),
                          ))
                    ],
                  ),
                ],
                SizedBox(height: 30),
                Btn(
                    child: Text("Simpan Data",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.nunito(
                            color: Colors.black,
                            fontSize: 15,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.w600)),
                    function: () {
                      update();
                    },
                    radius: 7,
                    elevated: 0.5,
                    clr: Clr.primaryButton,
                    outline: Clr.primaryButton,
                    width: displayWidth(context))
              ]))),
    );
  }
}
