// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kmf_app/core/bottom.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/create.dart';
import 'package:kmf_app/view/anggota/detail.dart';
import 'package:kmf_app/view/transaction/cart.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/view/widget/modal/standart.dart';
import 'package:kmf_app/view/widget/reject.dart';
import 'package:kmf_app/view/widget/waitwidget.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class IndexTransactionView extends StatefulWidget {
  @override
  State<IndexTransactionView> createState() => _IndexTransactionViewState();
}

class _IndexTransactionViewState extends State<IndexTransactionView> {
  List data = [];
  bool loading = true;
  getData() {
    AuthVM.product().then((value) {
      print("value $value");
      setState(() {
        loading = false;
        data = value["product"];
      });
    });
  }

  List<String> idData = [];
  List selectedData = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      // bottomNavigationBar: Navbar(1),
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.only(
                top: 5,
                right: displayWidth(context) * 0.05,
                left: displayWidth(context) * 0.01),
            child: Text(
              "Cart Screen",
              textAlign: TextAlign.start,
              style: GoogleFonts.poppins(
                  color: Color(0xff4D4D4D),
                  fontSize: 18,
                  letterSpacing: 0.25,
                  fontWeight: FontWeight.w700),
            ),
          ),
          leadingWidth: displayWidth(context) * 0,
          elevation: 0,
          actions: [TextButton(onPressed: () {}, child: Text("Report List"))],
          backgroundColor: Colors.white),
      body: Stack(
        children: [
          Container(
            color: Clr.primary,
            height: displayHeight(context),
            width: displayWidth(context),
            // padding: EdgeInsets.zero,
            padding:
                EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.05),
            child: ListView(
              children: [
                SizedBox(height: 20),
                for (var a = 0; a < data.length; a++) ...[
                  InkWell(
                    onTap: () {
                      int index = idData
                          .indexWhere((y) => y == data[a]['id'].toString());
                      setState(() {
                        if (index == -1) {
                          selectedData.add(data[a]);
                          idData.add(data[a]['id'].toString());
                        } else {
                          selectedData.removeAt(index);
                          idData.removeAt(index);
                        }
                      });
                      print("idData $idData");
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 7.5),
                      padding: EdgeInsets.symmetric(
                          vertical: 12.5,
                          horizontal: displayWidth(context) * 0.04),
                      color: Colors.white,
                      width: displayWidth(context),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data[a]['product_name'],
                                style: GoogleFonts.varela(
                                    color: Colors.black,
                                    fontSize: 17,
                                    letterSpacing: 1,
                                    fontWeight: FontWeight.w600),
                              ),
                              if (data[a]['discount'] != 0) ...[
                                Text(
                                  "Diskon " +
                                      NumberFormat.currency(
                                              locale: 'id',
                                              decimalDigits: 0,
                                              symbol: 'Rp ')
                                          .format(data[a]['price'])
                                          .toString(),
                                  style: GoogleFonts.varela(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black,
                                      fontSize: 15,
                                      letterSpacing: 0.25,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                              Text(
                                NumberFormat.currency(
                                        locale: 'id',
                                        decimalDigits: 0,
                                        symbol: 'Rp ')
                                    .format(data[a]['fixPrice'])
                                    .toString(),
                                style: GoogleFonts.varela(
                                    color: Colors.black,
                                    fontSize: 15,
                                    letterSpacing: 0.25,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          Icon(
                            idData.indexWhere(
                                        (y) => y == data[a]['id'].toString()) ==
                                    -1
                                ? Icons.circle_outlined
                                : Icons.circle,
                            color: idData.indexWhere(
                                        (y) => y == data[a]['id'].toString()) ==
                                    -1
                                ? Colors.black
                                : Clr.primaryButton,
                          )
                        ],
                      ),
                    ),
                  )
                ],
                SizedBox(height: 75),
              ],
            ),
          ),
          if (idData.length != 0) ...[
            Positioned(
                bottom: 0,
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: displayWidth(context) * 0.05),
                  // ignore: sort_child_properties_last
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${idData.length} Terpilih",
                        style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: 17,
                            letterSpacing: 0.2,
                            fontWeight: FontWeight.w500),
                      ),
                      Btn(
                          child: Text(
                            "Checkout",
                            style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontSize: 17,
                                letterSpacing: 0.2,
                                fontWeight: FontWeight.w500),
                          ),
                          function: () {
                            Routes.stf(
                                context,
                                CartTransactionView(
                                  dataCode: idData,
                                  data: selectedData,
                                ));
                          },
                          clr: Clr.primaryButton,
                          outline: Clr.primaryButton,
                          radius: 7.5,
                          height: 40,
                          width: displayWidth(context) * 0.35)
                    ],
                  ),
                  height: 75,
                  width: displayWidth(context),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(width: 1, color: Colors.black26))),
                ))
          ]
        ],
      ),
    );
  }
}
