import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/widget/inputs.dart';

class CadCart extends StatefulWidget {
  final Map data;
  final Function onChanged;
  final Function onChangedQty;
  const CadCart(
      {Key? key,
      required this.data,
      required this.onChanged,
      required this.onChangedQty})
      : super(key: key);

  @override
  State<CadCart> createState() => _CadCartState();
}

class _CadCartState extends State<CadCart> {
  TextEditingController qty = TextEditingController();
  int price = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: 15, horizontal: displayWidth(context) * 0.05),
      margin: EdgeInsets.only(bottom: 7.5),
      // height: 100,
      color: Colors.white,
      width: displayWidth(context),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.data['product_name'],
                style: GoogleFonts.varela(
                    color: Colors.black,
                    fontSize: 17,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                NumberFormat.currency(
                        locale: 'id', decimalDigits: 0, symbol: 'Rp ')
                    .format(widget.data['fixPrice'])
                    .toString(),
                style: GoogleFonts.varela(
                    color: Colors.black,
                    fontSize: 15,
                    letterSpacing: 0.25,
                    fontWeight: FontWeight.w500),
              ),
              Container(
                margin: EdgeInsets.only(top: 15, bottom: 25),
                height: 50,
                width: displayWidth(context) * 0.7,
                child: InputWidgetChg(
                    type: TextInputType.number,
                    fun: (xx) {
                      setState(() {
                        if (xx == '') {
                          widget.onChangedQty(0);
                          price = 0;
                        } else {
                          price = int.parse(xx) *
                              int.parse(widget.data['fixPrice'].toString());
                          widget.onChangedQty(xx);
                        }
                        widget.onChanged(price);
                      });
                    },
                    controller: qty,
                    usePreffix: false,
                    label: 'Kuantiti',
                    icon: 'asset/icon/auth/'),
              ),
              Text(
                "Sub total : ${NumberFormat.currency(locale: 'id', decimalDigits: 0, symbol: 'Rp ').format(price).toString()}",
                style: GoogleFonts.varela(
                    color: Colors.black,
                    fontSize: 17,
                    letterSpacing: 1,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
