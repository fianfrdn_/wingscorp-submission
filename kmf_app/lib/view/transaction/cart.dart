// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kmf_app/core/bottom.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/create.dart';
import 'package:kmf_app/view/anggota/detail.dart';
import 'package:kmf_app/view/transaction/widget/card.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/view/widget/modal/standart.dart';
import 'package:kmf_app/view/widget/reject.dart';
import 'package:kmf_app/view/widget/waitwidget.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class CartTransactionView extends StatefulWidget {
  final List data;
  final List<String> dataCode;

  const CartTransactionView(
      {super.key, required this.data, required this.dataCode});
  @override
  State<CartTransactionView> createState() => _CartTransactionViewState();
}

class _CartTransactionViewState extends State<CartTransactionView> {
  void openModal(String title, val) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ModalStdLog(title, val));
  }

  List<int> prc = [0];
  List<String> qty = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      // bottomNavigationBar: Navbar(1),
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.only(
                top: 5,
                right: displayWidth(context) * 0.05,
                left: displayWidth(context) * 0.01),
            child: Text(
              "Cart Screen",
              textAlign: TextAlign.start,
              style: GoogleFonts.poppins(
                  color: Color(0xff4D4D4D),
                  fontSize: 18,
                  letterSpacing: 0.25,
                  fontWeight: FontWeight.w700),
            ),
          ),
          leadingWidth: displayWidth(context) * 0,
          elevation: 0,
          backgroundColor: Colors.white),
      body: Stack(
        children: [
          Container(
            color: Clr.primary,
            height: displayHeight(context),
            width: displayWidth(context),
            // padding: EdgeInsets.zero,
            padding:
                EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.05),
            child: ListView(
              children: [
                SizedBox(height: 20),
                for (var a = 0; a < widget.data.length; a++) ...[
                  CadCart(
                      data: widget.data[a],
                      onChangedQty: (v) {
                        setState(() {
                          qty.add(v.toString());
                        });
                      },
                      onChanged: (v) {
                        setState(() {
                          prc.add(v);
                        });
                      }),
                ]
              ],
            ),
          ),
          Positioned(
              bottom: 0,
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: displayWidth(context) * 0.05),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${NumberFormat.currency(locale: 'id', decimalDigits: 0, symbol: 'Rp ').format(prc.reduce((a, b) => a + b)).toString()}",
                      style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: 17,
                          letterSpacing: 0.2,
                          fontWeight: FontWeight.w500),
                    ),
                    Btn(
                        function: () {
                          AuthVM.checkout(widget.dataCode, qty,
                                  prc.reduce((a, b) => a + b).toString())
                              .then((value) {
                            openModal("Berhasil checkout",
                                "Data pembelian anda berhasil tersimpan");
                          });
                          // print(qty.toString());
                        },
                        clr: Clr.primaryButton,
                        outline: Clr.primaryButton,
                        radius: 7.5,
                        height: 40,
                        child: Text(
                          "Konfirmasi",
                          style: GoogleFonts.poppins(
                              color: Colors.white,
                              fontSize: 17,
                              letterSpacing: 0.2,
                              fontWeight: FontWeight.w500),
                        ),
                        width: displayWidth(context) * 0.35)
                  ],
                ),
                height: 75,
                width: displayWidth(context),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        top: BorderSide(width: 1, color: Colors.black26))),
              ))
        ],
      ),
    );
  }
}
