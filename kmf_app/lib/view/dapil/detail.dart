// ignore_for_file: prefer_const_constructors

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/home/barchart.dart';
import 'package:kmf_app/view/home/index.dart';
import 'package:kmf_app/view/widget/btnPillIcon.dart';
import 'package:kmf_app/view/widget/carddapil.dart';
import 'package:kmf_app/vm/anggota.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DetailDapilView extends StatefulWidget {
  final String id, val, typeCard;
  const DetailDapilView(
      {Key? key, required this.id, required this.val, required this.typeCard})
      : super(key: key);

  @override
  State<DetailDapilView> createState() => _DetailDapilViewState();
}

class _DetailDapilViewState extends State<DetailDapilView> {
  List<BarChartModel> data = [];
  var tabIcon = "bar";
  getData() {
    if (widget.typeCard == "Kec.") {
      AnggotaVM.getBarsDetail(widget.id).then((value) {
        print(value.toString());
        setState(() {
          for (var uhu in value['res']) {
            data.add(BarChartModel(
                id: uhu['id'].toString(),
                month: '',
                year: uhu['name'],
                financial: value['kip'].toString() == "1"
                    ? uhu['user_kip_count']
                    : uhu['user_count'],
                color: charts.ColorUtil.fromDartColor(
                    Color(int.parse(Clr.warna[Random().nextInt(11)])))));
          }
        });
      });
    } else if (widget.typeCard == "Desa") {
      AnggotaVM.getBarsDetailByKec(widget.id).then((value) {
        setState(() {
          for (var uhu in value['res']) {
            data.add(BarChartModel(
                id: "XXX",
                month: '',
                year: uhu['name'],
                financial: value['kip'].toString() == "1"
                    ? uhu['user_kip_count']
                    : uhu['user_count'],
                color: charts.ColorUtil.fromDartColor(
                    Color(int.parse(Clr.warna[Random().nextInt(11)])))));
          }
        });
      });
    } else if (widget.typeCard == "Simpul") {
      // AnggotaVM.detailSimpul(widget.id).then((value) {
      //   setState(() {
      //     for (var uhu in value['data']) {
      //       data.add(BarChartModel(
      //           id: uhu['dapil'].toString(),
      //           month: '',
      //           year: uhu['name'],
      //           financial: uhu['user_count'],
      //           color: charts.ColorUtil.fromDartColor(
      //               Color(int.parse(Clr.warna[Random().nextInt(11)])))));
      //     }
      //   });
      // });
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 0,
        elevation: 1.25,
        shadowColor: Colors.black54,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: displayWidth(context) * 0.075,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('asset/icon/back.png'))),
              ),
            ),
            SizedBox(width: displayWidth(context) * 0.05),
            Text(
              "${widget.val}",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.04),
        color: Colors.white,
        width: displayWidth(context),
        height: displayHeight(context),
        child: ListView(children: [
          SizedBox(height: 20),
          SizedBox(
            width: displayWidth(context) * 0.187,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                    onTap: () {
                      setState(() {
                        tabIcon = "bar";
                      });
                    },
                    child: BtnPillIcon(val: "bar", active: tabIcon == "bar")),
                InkWell(
                    onTap: () {
                      setState(() {
                        tabIcon = "nonbar";
                      });
                    },
                    child: BtnPillIcon(
                        val: "nonbar", active: tabIcon == "nonbar")),
              ],
            ),
          ),
          SizedBox(height: 20),
          if (tabIcon == "bar") ...[
            BarChartGraph(data: data),
          ] else ...[
            for (var hh in data) ...[
              CardDapil(
                  id: hh.id,
                  num: hh.financial.toString(),
                  val: hh.year,
                  type: widget.typeCard),
            ]
          ],
        ]),
        // color: C,
      ),
    );
  }
}
