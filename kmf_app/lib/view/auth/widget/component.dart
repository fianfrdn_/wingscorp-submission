// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/size.dart';

class Component {
  static Widget auth(BuildContext context, {bool std = true}) {
    return SizedBox(
        height: displayHeight(context),
        width: displayWidth(context),
        child: Stack(children: [
          Positioned(
              top: !std ? -100 : displayHeight(context) * 0.04,
              child: Container(
                  height: !std
                      ? displayHeight(context) * 1.1
                      : displayHeight(context) * 0.9,
                  width: displayWidth(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage("asset/bg/bg-0001.png"))))),
          if (std) ...[
            Positioned(
                top: displayHeight(context) * 0.32,
                child: Container(
                    height: displayHeight(context) * 0.75,
                    width: displayWidth(context),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage("asset/bg/bg-0002.png"))))),
            Positioned(
                bottom: displayHeight(context) * 0.035,
                // right: displayWidth(context) * 0.01,
                child: SizedBox(
                  width: displayWidth(context),
                  child: Center(
                    child: Column(
                      children: [
                        Text("Dinaungi Oleh",
                            style: GoogleFonts.poppins(
                                color: Color(0xff000000),
                                fontSize: 13.5,
                                letterSpacing: 0.4,
                                fontWeight: FontWeight.bold)),
                        SizedBox(height: 10),
                        SizedBox(
                          width: displayWidth(context),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  height: 65,
                                  width: 90,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.fitWidth,
                                          image: AssetImage(
                                              "asset/icon/auth/2.png")))),
                              Container(
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.fitWidth,
                                          image: AssetImage(
                                              "asset/icon/auth/1.png")))),
                              SizedBox(width: 20),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ))
          ]
        ]));
  }
}
