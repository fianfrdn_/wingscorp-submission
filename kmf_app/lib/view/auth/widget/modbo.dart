// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/create.dart';
import 'package:kmf_app/view/auth/register.dart';

class ModalBottomAuths extends StatefulWidget {
  final List data;
  final String type;
  const ModalBottomAuths({Key? key, required this.data, this.type = ""})
      : super(key: key);

  @override
  State<ModalBottomAuths> createState() => _ModalBottomAuthsState();
}

class _ModalBottomAuthsState extends State<ModalBottomAuths> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: displayHeight(context) ,
      color: Colors.black54,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16), topRight: Radius.circular(16))),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 10),
            Center(
                child: Container(
              height: 0.75,
              margin: EdgeInsets.only(top: 1.75),
              width: displayWidth(context) * 0.235,
              color: Colors.black87,
            )),
            Center(
                child: Container(
              height: 0.75,
              margin: EdgeInsets.only(top: 1.75),
              width: displayWidth(context) * 0.25,
              color: Colors.black87,
            )),
            Center(
                child: Container(
              height: 0.75,
              margin: EdgeInsets.only(top: 1.75),
              width: displayWidth(context) * 0.235,
              color: Colors.black87,
            )),
            SizedBox(
              height: displayHeight(context) * 0.6,
              width: displayWidth(context),
              child: Column(
                children: [
                  SizedBox(height: 30),
                  TextFormField(),
                  Container(
                    height: displayHeight(context) * 0.475,
                    width: displayWidth(context),
                    child: ListView(
                      children: [
                        SizedBox(height: 15),
                        for (var a in widget.data) ...[
                          InkWell(
                              onTap: () {
                                if (widget.type == "") {
                                } else if (widget.type == "onCreateAnggota") {
                                  Navigator.pop(
                                      context,
                                      CreateAnggotaView(
                                        select: a["id"].toString(),
                                      ));
                                }
                              },
                              child: dbr(a["name"]))
                        ]
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget dbr(String val) {
    return Container(
      height: 45,
      width: displayWidth(context),
      margin: EdgeInsets.only(
          right: displayWidth(context) * 0.04,
          bottom: 10,
          left: displayWidth(context) * 0.04),
      // color: Colors.red,
      child: Row(
        children: [
          Icon(
            Icons.circle_outlined,
            color: Colors.blue,
          ),
          SizedBox(width: displayWidth(context) * 0.02),
          Text(val,
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w500)),
        ],
      ),
    );
  }
}
