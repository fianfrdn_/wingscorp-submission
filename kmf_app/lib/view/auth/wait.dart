// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/auth/widget/component.dart';

class WaitAuthView extends StatefulWidget {
  const WaitAuthView({Key? key}) : super(key: key);

  @override
  State<WaitAuthView> createState() => _WaitAuthViewState();
}

class _WaitAuthViewState extends State<WaitAuthView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFF3D4),
      appBar: AppBar(title: Text("WAITING")),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        padding: EdgeInsets.zero,
        child: Stack(children: [
          Component.auth(context),
        ]),
      ),
    );
  }
}
