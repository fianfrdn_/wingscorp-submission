// ignore_for_file: prefer_const_constructors, sort_child_properties_last, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/auth/register.dart';
import 'package:kmf_app/view/auth/wait.dart';
import 'package:kmf_app/view/auth/widget/component.dart';
import 'package:kmf_app/view/home/index.dart';
import 'package:kmf_app/view/transaction/index.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/view/widget/modal/standart.dart';
import 'package:kmf_app/vm/auth.dart';

class LoginAuthView extends StatefulWidget {
  const LoginAuthView({Key? key}) : super(key: key);

  @override
  State<LoginAuthView> createState() => _LoginAuthViewState();
}

class _LoginAuthViewState extends State<LoginAuthView> {
  bool obsecureText = true;
  TextEditingController nik = TextEditingController();
  TextEditingController pass = TextEditingController();
  bool loading = false;
  void _showErrorDialog(String title, val) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ModalStdLog(title, val));
  }

  void log() {
    setState(() {
      setState(() {
        loading = true;
      });
    });
    AuthVM.login(nik.text, pass.text).then((value) {
      if (value['status'] == "waiting") {
        Routes.stf(context, WaitAuthView());
      } else if (value['status'] == "error") {
        _showErrorDialog("Gagal Login", value['msg'].toString());
      } else if (value['status'] == "ok") {
        Routes.stf(context, IndexTransactionView());
      }
      setState(() {
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFF3D4),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        padding: EdgeInsets.zero,
        child: Stack(children: [
          // Component.auth(context),
          SafeArea(
              child: Padding(
            padding: EdgeInsets.fromLTRB(24, 40, 24, 0),
            child: ListView(
              children: [
                SizedBox(height: displayHeight(context) * 0.075),
                Container(
                  height: displayHeight(context) * 0.2,
                  child: Column(
                    children: [
                      SizedBox(height: displayHeight(context) * 0.1),
                      Text(
                        "Apps Penjualan\nWingsGroup",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xff4D4D4D),
                            fontSize: 26,
                            letterSpacing: 0.4,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: displayHeight(context) * 0.05),
                Container(
                  padding: EdgeInsets.only(
                      top: 20,
                      bottom: 30,
                      left: displayWidth(context) * 0.04,
                      right: displayWidth(context) * 0.04),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Column(
                    children: [
                      InputWidget(
                          controller: nik,
                          label: 'Username',
                          icon: 'asset/icon/auth/nik.png'),
                      SizedBox(height: 17.5),
                      InputWidgetFun(
                          fun: () {
                            setState(() {
                              obsecureText = !obsecureText;
                            });
                          },
                          obsecureText: obsecureText,
                          useSuffix: true,
                          suffixIcon: Icon(Icons.remove_red_eye),
                          controller: pass,
                          label: 'Password',
                          icon: 'asset/icon/auth/password.png'),
                      SizedBox(height: 20),
                      Btn(
                          child: Text(
                            loading ? "Loading Masuk" : "Masuk",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          radius: 10,
                          function: () {
                            if (!loading) {
                              log();
                            }
                          },
                          clr: Clr.primaryButton,
                          outline: Clr.primaryButton,
                          elevated: 1.5,
                          width: displayWidth(context)),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Aplikasi submisi",
                              style: GoogleFonts.poppins(
                                color: Color(0xff4D4D4D),
                                fontSize: 13.5,
                                letterSpacing: 0.4,
                              )),
                          SizedBox(width: 3),
                          InkWell(
                            child: Text(
                              "WingsCorp",
                              style: GoogleFonts.poppins(
                                  color: Clr.primaryButton,
                                  fontSize: 14.5,
                                  letterSpacing: 0.4,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ))
        ]),
      ),
    );
  }
}
