// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/auth/login.dart';
import 'package:kmf_app/view/auth/widget/component.dart';
import 'package:kmf_app/view/home/index.dart';
import 'package:kmf_app/view/transaction/index.dart';
import 'package:kmf_app/vm/auth.dart';

class SplashAuthView extends StatefulWidget {
  const SplashAuthView({Key? key}) : super(key: key);

  @override
  State<SplashAuthView> createState() => _SplashAuthViewState();
}

class _SplashAuthViewState extends State<SplashAuthView> {
  starts() {
    // if (mounted) {
    AuthVM.checkAuth().then((value) {
      if (value == "home") {
        Routes.stfR(context, IndexTransactionView());
      } else {
        Routes.stfR(context, LoginAuthView());
      }
    });
    // }
  }

  @override
  void initState() {
    starts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFF3D4),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        padding: EdgeInsets.zero,
      ),
    );
  }
}
