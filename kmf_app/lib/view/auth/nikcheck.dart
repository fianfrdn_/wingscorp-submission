// ignore_for_file: prefer_const_constructors, sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/auth/widget/component.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/vm/auth.dart';

class NikCheckAuthView extends StatefulWidget {
  final String nik;
  const NikCheckAuthView({Key? key, required this.nik}) : super(key: key);

  @override
  State<NikCheckAuthView> createState() => _NikCheckAuthViewState();
}

class _NikCheckAuthViewState extends State<NikCheckAuthView> {
  bool loadingAPI = true;
  bool adaNIK = true;
  Map data = {};
  checkNik() {
    // AuthVM.nikCheck(widget.nik).then((value) {
    //   print("value : $value");
    //   setState(() {
    //     if (value.toString().contains("belum")) {
    //       adaNIK = false;
    //     }
    //     data = value;
    //     loadingAPI = false;
    //   });
    // });
  }

  @override
  void initState() {
    checkNik();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffFFF3D4),
        body: Container(
            height: displayHeight(context),
            width: displayWidth(context),
            padding: EdgeInsets.zero,
            child: Stack(children: [
              Component.auth(context),
              SafeArea(
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(15, 25, 15, 0),
                      child: ListView(children: [
                        Container(
                            padding: EdgeInsets.only(
                                top: 20,
                                bottom: 30,
                                left: displayWidth(context) * 0.04,
                                right: displayWidth(context) * 0.04),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // InkWell(
                                  //   onTap: () {
                                  //     Navigator.pop(context);
                                  //   },
                                  //   child: Container(
                                  //     width: displayWidth(context) * 0.075,
                                  //     height: 40,
                                  //     decoration: BoxDecoration(
                                  //         image: DecorationImage(
                                  //             image: AssetImage(
                                  //                 'asset/icon/back.png'))),
                                  //   ),
                                  // ),
                                  Text(
                                      adaNIK
                                          ? "NIK Telah Terdaftar"
                                          : "NIK Belum Terdaftar",
                                      style: GoogleFonts.nunito(
                                          color: Colors.black,
                                          fontSize: 17.5,
                                          fontWeight: FontWeight.w600)),
                                  SizedBox(height: 10),
                                  Text(
                                      adaNIK
                                          ? "NIK anda telah di daftarkan sebelumnya oleh koordinator cabang dan untuk info lengkap silahkan hubungi :"
                                          : "Anda dapat menggunakan NIK yang anda inputkan karena masih belum terdaftar di sistem. Klik tombol KEMBALI untuk melanjutkan registrasi.",
                                      textAlign: TextAlign.start,
                                      style: GoogleFonts.nunito(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600)),
                                  if (adaNIK && !loadingAPI) ...[
                                    SizedBox(height: 20),
                                    lr("Koor Cab.", data["success"]["name"]),
                                    lr("NoHP", data["success"]["nohp"]),
                                    lr("Alamat", data["success"]["alamat"]),
                                  ],
                                  SizedBox(height: 20),
                                  Btn(
                                      child: Text("Kembali",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600)),
                                      radius: 10,
                                      height: 40,
                                      function: () {
                                        Navigator.pop(context);
                                      },
                                      clr: Clr.primaryButton,
                                      outline: Clr.primaryButton,
                                      elevated: 1.5,
                                      width: displayWidth(context))
                                ]))
                      ])))
            ])));
  }

  Widget lr(l, r) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: displayWidth(context) * 0.25,
          child: Text("$l",
              style: GoogleFonts.nunito(
                  color: Colors.black,
                  fontSize: 14.5,
                  fontWeight: FontWeight.w600)),
        ),
        SizedBox(
          width: displayWidth(context) * 0.05,
          child: Text(":",
              style: GoogleFonts.nunito(
                  color: Colors.black,
                  fontSize: 14.5,
                  fontWeight: FontWeight.w600)),
        ),
        SizedBox(
          width: displayWidth(context) * 0.45,
          child: Text("$r",
              style: GoogleFonts.nunito(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600)),
        ),
      ],
    );
  }
}
