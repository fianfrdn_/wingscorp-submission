import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kmf_app/core/size.dart';

class ChartCustom extends StatelessWidget {
  const ChartCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      width: displayWidth(context),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(8.5),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            width: displayWidth(context) * 0.125,
            child: Column(
              children: [Text("A")],
            ),
          ),
          // Divider()
          VerticalDivider(color: Colors.black),
          SizedBox(width: displayWidth(context) * 0.7)
        ],
      ),
    );
  }
}
