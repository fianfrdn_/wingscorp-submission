// ignore_for_file: prefer_const_constructors, sort_child_properties_last
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/bottom.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/detail.dart';
import 'package:kmf_app/view/home/barchart.dart';
import 'package:kmf_app/view/profile/index.dart';
import 'package:kmf_app/view/widget/btnPillIcon.dart';
import 'package:kmf_app/view/widget/carddapil.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:kmf_app/view/widget/cardsimpul.dart';
import 'package:kmf_app/view/widget/reject.dart';
import 'package:kmf_app/view/widget/waitwidget.dart';
import 'package:kmf_app/vm/anggota.dart';
import 'package:kmf_app/vm/spf.dart';

import '../auth/widget/component.dart';

class IndexHomeView extends StatefulWidget {
  const IndexHomeView({Key? key}) : super(key: key);

  @override
  State<IndexHomeView> createState() => _IndexHomeViewState();
}

class _IndexHomeViewState extends State<IndexHomeView> {
  final List<BarChartModel> data = [];
  Map tagUser = {};
  bool loadingTagUser = true;
  // List barData = [];
  var tabChart = "LOADING";
  var tabIcon = "bar";
  getData({required String id}) async {
    if (id == "Simpul") {
      AnggotaVM.simpulHome().then((value) {
        chchc(value);
      });
    } else {
      AnggotaVM.getBars(id).then((value) {
        chchc(value);
      });
    }
  }

  chchc(var value) {
    data.clear();
    setState(() {
      int i = 0;
      value.forEach((k, v) {
        i++;
        // print("K: $k V $v");
        data.add(BarChartModel(
            id: v["dapil"].toString(),
            month: '',
            year: k,
            financial: v["sum_user"],
            color: charts.ColorUtil.fromDartColor(
                Color(int.parse(Clr.warna[i])))));
      });
    });
  }

  String role = '';
  String status = '';
  String rejects = '';
  String id = '';
  String kip = 'null';
  Future getCurrentTMP() async {
    String tmp = await SP.getOthers("role") ?? "-";
    String kota_id = await SP.getOthers("kota_id") ?? "-";
    String kecamatan_id = await SP.getOthers("kecamatan_id") ?? "-";
    String desa_id = await SP.getOthers("desa_id") ?? "-";
    String rejectsD = await SP.getOthers("rejects") ?? "-";
    String statusD = await SP.getOthers("status") ?? "-";
    String idD = await SP.getOthers("id") ?? "-";
    String nameTMP = await SP.getOthers("name") ?? "-";
    String nikTMP = await SP.getOthers("nik") ?? "-";
    String kipTMP = await SP.getOthers("kip") ?? "null";
    setState(() {
      kip = kipTMP;
      id = idD;
      status = statusD;
      tagUser["name"] = nameTMP;
      tagUser["nik"] = nikTMP;
      rejects = rejectsD;
      role = tmp;
      tabChart = kota_id == "null" ? "3509" : kota_id;
      loadingTagUser = false;
      // if (tmp == "korcam") {
      // }
    });
  }

  @override
  void initState() {
    getCurrentTMP().then((value) {
      if (status == "1") {
        getData(id: tabChart);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar:
          (role == "user" && kip != "1") ? SizedBox() : Navbar(0),
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.only(
                top: 5,
                right: displayWidth(context) * 0.05,
                left: displayWidth(context) * 0.01),
            child: InkWell(
              onTap: () {},
              child: Text(
                "Hai, $role 👋",
                textAlign: TextAlign.start,
                style: GoogleFonts.poppins(
                    color: Color(0xff4D4D4D),
                    fontSize: 18,
                    letterSpacing: 0.25,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),
          leadingWidth: displayWidth(context) * 0,
          elevation: 0,
          backgroundColor: Colors.white),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        padding: EdgeInsets.zero,
        child: Stack(
          children: [
            Component.auth(context, std: false),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: displayWidth(context) * 0.05),
              child: ListView(
                children: [
                  SizedBox(height: 10),
                  Container(
                    height: 115,
                    width: displayWidth(context),
                    // color: Colors.red,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("asset/bgpur.png"))),
                  ),
                  SizedBox(height: 35),
                  InkWell(
                    onTap: () {
                      // Routes.stf(
                      // context, DetailAnggotaView(id: id, current: true));
                      Routes.stf(context, IndexProfileView());
                    },
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: displayWidth(context) * 0.65,
                            height: 75,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: displayWidth(context) * 0.04),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    loadingTagUser ? "Memuat" : tagUser["name"],
                                    style: TextStyle(
                                        color: Color(0xff4D4D4D),
                                        fontSize: 15,
                                        letterSpacing: 0.4,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    loadingTagUser ? "Memuat" : tagUser["nik"],
                                    style: TextStyle(
                                        color: Color(0xff4D4D4D),
                                        fontSize: 13,
                                        letterSpacing: 0.4,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: displayWidth(context) * 0.125,
                            // height: 75,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  right: displayWidth(context) * 0.04),
                              child: Container(
                                height: 24,
                                width: displayWidth(context) * 0.015,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image:
                                            AssetImage("asset/icon/ntg.png"))),
                              ),
                            ),
                          )
                        ],
                      ),
                      height: 75,
                      width: displayWidth(context),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border.all(width: 1, color: Color(0xffEDEDED)),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                    ),
                  ),
                  SizedBox(height: 15),
                  if (status == "1") ...[
                    SizedBox(
                      width: displayWidth(context),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: displayWidth(context) * 0.65,
                            height: 35,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        tabChart = "3509";
                                      });
                                      getData(id: "3509");
                                    },
                                    child:
                                        btPill("Jember", tabChart == "3509")),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        tabChart = "3508";
                                      });
                                      getData(id: "3508");
                                    },
                                    child:
                                        btPill("Lumajang", tabChart == "3508")),

                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        tabChart = "Simpul";
                                      });
                                      getData(id: "Simpul");
                                    },
                                    child:
                                        btPill("Simpul", tabChart == "Simpul")),

                                // Center(child: btPill("Simpul", false)),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: displayWidth(context) * 0.187,
                            child: Row(
                              children: [
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        tabIcon = "bar";
                                      });
                                    },
                                    child: btPillIcon("bar", tabIcon == "bar")),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        tabIcon = "nonbar";
                                      });
                                    },
                                    child: btPillIcon(
                                        "nonbar", tabIcon == "nonbar")),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    if (tabIcon == "bar") ...[
                      BarChartGraph(data: data),
                    ] else ...[
                      for (var hh in data) ...[
                        if (tabChart != "Simpul") ...[
                          CardDapil(
                              type: "dapil",
                              id: hh.id,
                              num: hh.financial.toString(),
                              val: hh.year),
                        ] else ...[
                          CardSimpul(
                              programID: hh.id,
                              typeSimpul: "kota",
                              val: hh.year,
                              num: hh.financial.toString(),
                              id: hh.id,
                              type: "simpul"),
                        ]
                      ]
                    ]
                  ] else if (status == "0") ...[
                    WaitWidget()
                  ] else if (status == "2") ...[
                    RejectWidget(val: rejects)
                  ]
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget btPill(String val, active) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
          color: active ? Color(0xffFF9900) : Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6.5))),
      padding: EdgeInsets.symmetric(
          vertical: 7.5, horizontal: displayWidth(context) * 0.025),
      alignment: Alignment.center,
      child: Text(
        val,
        textAlign: TextAlign.start,
        style: TextStyle(
            color: !active ? Color(0xffFF9900) : Colors.white,
            fontSize: 15,
            letterSpacing: 0.3,
            fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget btPillIcon(String val, active) {
    return BtnPillIcon(active: active, val: val);
  }
}

class BarChartModel {
  String month, id;
  String year;
  int financial;
  final charts.Color color;

  BarChartModel({
    required this.id,
    required this.month,
    required this.year,
    required this.financial,
    required this.color,
  });
}
