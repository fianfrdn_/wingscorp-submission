import 'package:flutter/cupertino.dart';
import 'package:kmf_app/core/size.dart';

class Decors {
  static EdgeInsetsGeometry regularPadding(BuildContext context) {
    return EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.04);
  }

  static BorderRadiusGeometry regularRadius({double w = 7.5}) {
    return BorderRadius.all(Radius.circular(w));
  }
}
