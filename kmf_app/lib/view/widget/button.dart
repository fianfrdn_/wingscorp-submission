import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final Function function;
  final Color clr, outline;
  final double width, height, radius, elevated;
  final Widget child;
  const Btn(
      {Key? key,
      required this.function,
      required this.clr,
      required this.outline,
      required this.width,
      this.height = 50,
      this.radius = 100,
      this.elevated = 15,
      this.child = const SizedBox()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: height,
        width: width,
        child: ElevatedButton(
            style: ButtonStyle(
                elevation: MaterialStateProperty.all<double>(elevated),
                foregroundColor: MaterialStateProperty.all<Color>(clr),
                backgroundColor: MaterialStateProperty.all<Color>(clr),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(radius),
                        side: BorderSide(color: outline)))),
            onPressed: () {
              function();
            },
            child: child));
  }
}
