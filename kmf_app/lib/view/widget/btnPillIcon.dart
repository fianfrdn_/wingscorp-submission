import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kmf_app/core/size.dart';

class BtnPillIcon extends StatelessWidget {
  final bool active;
  final String val;
  const BtnPillIcon({Key? key, required this.active, required this.val})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
          color: active ? Color(0xffFF9900) : Colors.transparent,
          borderRadius: BorderRadius.all(Radius.circular(6.5))),
      padding: EdgeInsets.symmetric(
          vertical: 8.5, horizontal: displayWidth(context) * 0.02),
      child: Container(
        height: 16,
        width: 16,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("asset/icon/$val-$active.png"))),
      ),
    );
  }
}
