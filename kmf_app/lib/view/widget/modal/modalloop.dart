// ignore_for_file: prefer_const_constructors_in_immutables, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/widget/decors.dart';

class ModalLoop extends StatefulWidget {
  final String title;
  final Map message;
  ModalLoop(this.title, this.message);
  @override
  State<ModalLoop> createState() => _ModalLoopState();
}

class _ModalLoopState extends State<ModalLoop> {
  List validator = [];
  setData() {
    widget.message.entries.forEach((element) {
      for (var k in element.value) {
        setState(() {
          validator.add(k);
        });
      }
      // print("EL element.key ${element.key} element.value ${element.value}");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    setData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Dialog(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(8),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: Offset(0.0, 10.0))
                ]),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: displayWidth(context) * 0.06,
                          right: displayWidth(context) * 0.06,
                          top: 30),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.title,
                              style: GoogleFonts.poppins(
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                fontSize: 14.5,
                                letterSpacing: 0.4,
                              ),
                            ),
                            const SizedBox(height: 10),
                            for (var f = 0; f < validator.length; f++) ...[
                              const SizedBox(height: 3.5),
                              Text(
                                "${(f + 1)} . " + validator[f].toString(),
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 13.5,
                                  letterSpacing: 0.4,
                                ),
                              ),
                            ],
                            const SizedBox(height: 15),
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                          width: displayWidth(context) * 0.65,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Clr.primaryButton,
                                                  width: 1),
                                              color: Clr.primaryButton,
                                              borderRadius:
                                                  Decors.regularRadius(w: 6.5)),
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "OK, Saya mengerti",
                                                  style: GoogleFonts.poppins(
                                                    color: Colors.black,
                                                    fontSize: 13.5,
                                                    letterSpacing: 0.4,
                                                  ),
                                                ),
                                              ])))
                                ]),
                            const SizedBox(height: 30)
                          ]))
                ])));
  }
}
