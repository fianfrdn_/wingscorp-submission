// ignore_for_file: prefer_const_constructors, sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/dapil/detailbydesa.dart';
import 'package:kmf_app/view/widget/button.dart';

class CardSimpul extends StatelessWidget {
  final String val, num, id, type, typeSimpul, programID;

  const CardSimpul(
      {super.key,
      required this.val,
      required this.num,
      required this.id,
      this.type = 'dapil',
      this.typeSimpul = '',
      required this.programID});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 12.5),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 1),
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(7.5))),
      padding: EdgeInsets.symmetric(
          vertical: 10, horizontal: displayWidth(context) * 0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            val,
            textAlign: TextAlign.start,
            style: const TextStyle(
                color: Colors.black,
                fontSize: 15,
                letterSpacing: 0.4,
                fontWeight: FontWeight.w600),
          ),
          Row(
            children: [
              Text(
                "$num Anggota",
                textAlign: TextAlign.start,
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    letterSpacing: 0.25,
                    fontWeight: FontWeight.w400),
              ),
              if (typeSimpul != "end") ...[
                SizedBox(width: displayWidth(context) * 0.025),
                Btn(
                    child: Text(
                      "Detail",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13.5,
                          letterSpacing: 0.35,
                          fontWeight: FontWeight.w500),
                    ),
                    function: () {
                      print(type);
                      Routes.stf(
                          context,
                          DetailSimpulView(
                              programID: programID,
                              id: id,
                              val: val,
                              typeCard: typeSimpul));
                    },
                    clr: Color(0xffFFB800),
                    outline: Color(0xffFFB800),
                    height: 26.5,
                    elevated: 0,
                    radius: 7.5,
                    width: displayWidth(context) * 0.1875)
              ]
            ],
          )
        ],
      ),
    );
  }
}
