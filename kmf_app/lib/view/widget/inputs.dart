// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/size.dart';

class InputWidget extends StatelessWidget {
  final IconData suffixIcon;
  final String label, icon;
  final TextEditingController controller;
  final bool enabled, usePreffix, useSuffix;
  final int maxLines;
  final TextInputType type;
  const InputWidget(
      {super.key,
      required this.label,
      required this.icon,
      required this.controller,
      this.enabled = true,
      this.usePreffix = true,
      this.useSuffix = false,
      this.suffixIcon = Icons.abc,
      this.maxLines = 1,
      this.type = TextInputType.name});

  @override
  Widget build(BuildContext context) {
    InputBorder asad = OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffEDEDED), width: 1.25));
    InputDecoration bof = InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 45),
        enabled: enabled,
        // prefixIcon: SvgPicture.asset("asset/icon/Vector.svg",
        //     color: Colors.black, height: 2, width: 2, fit: BoxFit.fitWidth),
        suffixIcon:
            useSuffix ? Icon(suffixIcon) : SizedBox(width: 0, height: 0),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle: GoogleFonts.nunito(
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    InputDecoration bog = InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            vertical: 10, horizontal: displayWidth(context) * 0.05),
        enabled: enabled,
        suffixIcon:
            useSuffix ? Icon(suffixIcon) : SizedBox(width: 0, height: 0),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle:
            GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w400),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(14)),
      child: Stack(
        children: [
          TextField(
              keyboardType: type,
              maxLines: maxLines,
              controller: controller,
              enabled: enabled,
              decoration: usePreffix ? bof : bog),
          if (usePreffix) ...[
            Positioned(
                top: 12.5,
                left: 12,
                child: Container(
                  height: 24,
                  width: 24,
                  decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(icon))),
                ))
          ]
        ],
      ),
    );
  }
}

class InputWidgetChg extends StatelessWidget {
  final IconData suffixIcon;
  final String label, icon;
  final TextEditingController controller;
  final bool enabled, usePreffix, useSuffix;
  final int maxLines;
  final TextInputType type;
  final Function fun;
  const InputWidgetChg(
      {super.key,
      required this.label,
      required this.icon,
      required this.controller,
      this.enabled = true,
      this.usePreffix = true,
      this.useSuffix = false,
      this.suffixIcon = Icons.abc,
      this.maxLines = 1,
      this.type = TextInputType.name,
      required this.fun});

  @override
  Widget build(BuildContext context) {
    InputBorder asad = OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffEDEDED), width: 1.25));
    InputDecoration bof = InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 45),
        enabled: enabled,
        // prefixIcon: SvgPicture.asset("asset/icon/Vector.svg",
        //     color: Colors.black, height: 2, width: 2, fit: BoxFit.fitWidth),
        suffixIcon:
            useSuffix ? Icon(suffixIcon) : SizedBox(width: 0, height: 0),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle: GoogleFonts.nunito(
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    InputDecoration bog = InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            vertical: 10, horizontal: displayWidth(context) * 0.05),
        enabled: enabled,
        suffixIcon:
            useSuffix ? Icon(suffixIcon) : SizedBox(width: 0, height: 0),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle:
            GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w400),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(14)),
      child: Stack(
        children: [
          TextField(
              onChanged: (r) {
                // fun(x) {
                // x = r;
                // }
                fun(r.toString());
                // print(r.toString());
              },
              keyboardType: type,
              maxLines: maxLines,
              controller: controller,
              enabled: enabled,
              decoration: usePreffix ? bof : bog)
        ],
      ),
    );
  }
}

class InputWidgetFun extends StatelessWidget {
  final Widget suffixIcon;
  final String label, icon;
  final TextEditingController controller;
  final bool enabled, usePreffix, useSuffix, obsecureText;
  final int maxLines;
  final Function fun;
  const InputWidgetFun(
      {super.key,
      required this.label,
      required this.icon,
      required this.controller,
      this.enabled = true,
      this.obsecureText = true,
      this.usePreffix = true,
      this.useSuffix = false,
      required this.suffixIcon,
      this.maxLines = 1,
      required this.fun});

  @override
  Widget build(BuildContext context) {
    InputBorder asad = OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffEDEDED), width: 1.25));
    InputDecoration bof = InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 45),
        enabled: enabled,
        // prefixIcon:
        // ImageIcon(AssetImage(icon), size: 24, color: Color(0xff4D4D4D)),
        suffixIcon: InkWell(
          onTap: () {
            fun();
          },
          child: suffixIcon,
        ),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle: GoogleFonts.nunito(
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    InputDecoration bog = InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            vertical: 10, horizontal: displayWidth(context) * 0.05),
        enabled: enabled,
        suffixIcon: InkWell(
          onTap: () {
            fun();
          },
          child: suffixIcon,
        ),
        disabledBorder: asad,
        focusedBorder: asad,
        enabledBorder: asad,
        hintText: label,
        hintStyle:
            GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w400),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide.none));

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(14)),
      child: Stack(
        children: [
          TextField(
              obscureText: obsecureText,
              maxLines: maxLines,
              controller: controller,
              enabled: enabled,
              decoration: usePreffix ? bof : bog),
          Positioned(
              top: 12.5,
              left: 12,
              child: Container(
                height: 24,
                width: 24,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(icon))),
              ))
        ],
      ),
    );
  }
}
