import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/size.dart';

class WaitWidget extends StatelessWidget {
  const WaitWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 50),
      width: displayWidth(context),
      color: Colors.white,
      child: Column(
        children: [
          Icon(Icons.watch_later_rounded, size: 72, color: Color(0xffC21807)),
          SizedBox(height: 12.5),
          Text(
            "Saat ini akun sedang dalam\nproses verifikasi",
            textAlign: TextAlign.center,
            style: GoogleFonts.poppins(
                color: Color(0xff4D4D4D),
                fontSize: 16,
                letterSpacing: 0.25,
                fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
