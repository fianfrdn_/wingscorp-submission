// ignore_for_file: prefer_const_constructors, sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kmf_app/core/bottom.dart';
import 'package:kmf_app/core/route.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/anggota/detail.dart';
import 'package:kmf_app/view/auth/login.dart';
import 'package:kmf_app/view/profile/change-pass.dart';
import 'package:kmf_app/view/widget/modal/standart.dart';
import 'package:kmf_app/vm/auth.dart';
import 'package:kmf_app/vm/spf.dart';

class IndexProfileView extends StatefulWidget {
  final String updated;
  const IndexProfileView({Key? key, this.updated = ""}) : super(key: key);

  @override
  State<IndexProfileView> createState() => _IndexProfileViewState();
}

class _IndexProfileViewState extends State<IndexProfileView> {
  TextEditingController search = TextEditingController();
  Map data = {};
  String tab = 'wait';
  bool loading = true;
  getData() {
    // AuthVM.verify(tab).then((value) {
    //   setState(() {
    //     data = value;
    //     loading = false;
    //   });
    // });
  }

  String kip = 'null';
  bool loadingTagUser = true;
  String role = '';
  String status = '';
  String id = '';
  Future getCurrentTMP() async {
    String idD = await SP.getOthers("id");
    String tmp = await SP.getOthers("role");
    String statusD = await SP.getOthers("status");
    String kipTMP = await SP.getOthers("kip") ?? "null";
    setState(() {
      kip = kipTMP;
      role = tmp;
      status = statusD;
      id = idD;
      loadingTagUser = false;
      // if (tmp == "korcam") {
      // }
    });
  }

  @override
  void initState() {
    getCurrentTMP().then((value) {});
    super.initState();
  }

  void modal(String title, val) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ModalStdLog(title, val));
  }

  routePage(BuildContext context) async {
    var stepPage =
        await Navigator.push(context, MaterialPageRoute(builder: (_) {
      return ChangePassProfileAuthView(id: id);
    }));
    try {
      final IndexProfileView args = stepPage;
      print("object $args");
      if (args.updated == "001") {
        modal("Berhasil", "Anda baru saja mengubah kata sandi akun anda !");
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      bottomNavigationBar:
          (role == "user" && kip != "1") ? SizedBox() : Navbar(2),
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Padding(
            padding: EdgeInsets.only(
                top: 5,
                right: displayWidth(context) * 0.05,
                left: displayWidth(context) * 0.01),
            child: Text(
              "Hai, $role 👋",
              textAlign: TextAlign.start,
              style: GoogleFonts.poppins(
                  color: Color(0xff4D4D4D),
                  fontSize: 18,
                  letterSpacing: 0.25,
                  fontWeight: FontWeight.w700),
            ),
          ),
          leadingWidth: displayWidth(context) * 0,
          elevation: 0,
          actions: [
            if (role == "user" && kip != "1") ...[
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Kembali"))
            ]
          ],
          backgroundColor: Colors.white),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        // padding: EdgeInsets.zero,
        padding: EdgeInsets.symmetric(horizontal: displayWidth(context) * 0.05),
        child: ListView(
          children: [
            SizedBox(height: 20),
            menus(
                func: () {
                  Routes.stf(
                      context,
                      DetailAnggotaView(
                        id: id,
                        current: true,
                      ));
                },
                imagePath: "asset/icon/auth/nohp.png",
                val: "Profil Saya"),
            SizedBox(height: 20),
            menus(
                func: () {
                  routePage(context);
                },
                imagePath: "asset/icon/auth/password.png",
                val: "Ganti password"),
            SizedBox(height: 20),
            menus(
                func: () {
                  AuthVM.saver("-", "-", "-", "-", "-");
                  Routes.stfR(context, LoginAuthView());
                },
                imagePath: "asset/icon/logout.png",
                val: "Logout"),
          ],
        ),
      ),
    );
  }

  Widget menus({String val = '', imagePath, required Function func}) {
    return InkWell(
      onTap: () {
        func();
      },
      child: Column(
        children: [
          Row(
            children: [
              Text("$val",
                  style: GoogleFonts.poppins(
                    color: Color(0xff000000),
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.4,
                  )),
              SizedBox(width: 10),
              Container(
                height: 20,
                width: 20,
                // color: Colors.red,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    image: DecorationImage(
                        fit: BoxFit.fitWidth, image: AssetImage(imagePath))),
              ),
            ],
          ),
          SizedBox(height: 5),
          Divider(
            height: 1,
            color: Colors.black,
            thickness: 1,
          )
        ],
      ),
    );
  }
}
