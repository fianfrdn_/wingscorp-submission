import 'package:flutter/material.dart';
import 'package:kmf_app/core/color.dart';
import 'package:kmf_app/core/size.dart';
import 'package:kmf_app/view/profile/index.dart';
import 'package:kmf_app/view/widget/button.dart';
import 'package:kmf_app/view/widget/inputs.dart';
import 'package:kmf_app/vm/auth.dart';

class ChangePassProfileAuthView extends StatefulWidget {
  final String id;
  const ChangePassProfileAuthView({Key? key, required this.id})
      : super(key: key);

  @override
  State<ChangePassProfileAuthView> createState() =>
      _ChangePassProfileAuthViewState();
}

class _ChangePassProfileAuthViewState extends State<ChangePassProfileAuthView> {
  bool obsecureTextPass1 = true;
  bool obsecureTextPass2 = true;
  bool loading = false;
  TextEditingController pass1 = TextEditingController();
  TextEditingController pass2 = TextEditingController();

  store() {
    setState(() {
      loading = true;
    });
    // AuthVM.changePassword(pass2.text).then((value) {
    //   print(value.toString());
    //   setState(() {
    //     loading = false;
    //   });
    //   Navigator.pop(context, IndexProfileView(updated: "001"));
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 0,
        elevation: 1.25,
        shadowColor: Colors.black54,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: displayWidth(context) * 0.075,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('asset/icon/back.png'))),
              ),
            ),
            SizedBox(width: displayWidth(context) * 0.05),
            Text(
              "Ubah Kata Sandi",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 0.4,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
      body: Container(
        height: displayHeight(context),
        width: displayWidth(context),
        child: ListView(
          children: [
            SizedBox(height: 27.5),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: displayWidth(context) * 0.045),
              padding: EdgeInsets.symmetric(
                  horizontal: displayWidth(context) * 0.03, vertical: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(width: 1, color: Colors.black26)),
              child: Column(
                children: [
                  SizedBox(height: 12.5),
                  InputWidgetFun(
                      fun: () {
                        setState(() {
                          obsecureTextPass1 = !obsecureTextPass1;
                        });
                      },
                      obsecureText: obsecureTextPass1,
                      useSuffix: true,
                      suffixIcon: Icon(Icons.remove_red_eye),
                      controller: pass1,
                      label: 'Password Baru',
                      icon: 'asset/icon/auth/password.png'),
                  SizedBox(height: 17.5),
                  InputWidgetFun(
                      fun: () {
                        setState(() {
                          obsecureTextPass2 = !obsecureTextPass2;
                        });
                      },
                      obsecureText: obsecureTextPass2,
                      useSuffix: true,
                      suffixIcon: Icon(Icons.remove_red_eye),
                      controller: pass2,
                      label: 'Ulangi Password Baru',
                      icon: 'asset/icon/auth/password.png'),
                  SizedBox(height: 27.5),
                  Btn(
                      child: Text(
                        loading ? "Loading ..." : "Ubah Sandi",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      radius: 10,
                      function: () {
                        if (!loading) {
                          store();
                        }
                      },
                      clr: Clr.primaryButton,
                      outline: Clr.primaryButton,
                      elevated: 1.5,
                      width: displayWidth(context)),
                  SizedBox(height: 20)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
