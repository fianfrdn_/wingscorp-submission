<?php

namespace App\Http\Controllers;
use Auth;
use \App\User;
use \App\Product;
use \App\TransactionHeader;
use \App\TransactionDetail;
use Illuminate\Http\Request;
use \App\Http\Controllers\Repository\UserRepository;
use \App\Http\Controllers\Repository\TransactionRepository;

class WebController extends Controller
{
    public function checkout()
    {
        $data['step'] = 1;
        $data['product'] = Product::All();
        return view('checkout/all', compact('data'));
    }
    public function store1(Request $r)
    {
        $data['step'] = 2;
        $data['product'] = Product::whereIn('id', $r->product_id)->get();
        return view('checkout/all', compact('data'));
    }
    public function store2(Request $r)
    {
        $data['step'] = 3;
        $data['kuantiti'] = $r->kuantiti;
        $data['product'] = Product::whereIn('id', $r->product_id)->get();
        return view('checkout/all', compact('data'));
    }
    public function store3(Request $r)
    {
        $th = TransactionHeader::create([
            'document_Code' => substr(md5(mt_rand(0,10)),0,3),
            'document_Number' => substr(md5(mt_rand(0,10)),0,10),
            'total' => $r->total,
            'user' => Auth::user()->id,
            'date' => \Carbon\Carbon::now(),
        ]);
        foreach ($r->prod_id as $i => $key) {
            $data = Product::whereProductCode($key)->first();
            TransactionDetail::create([
                "document_code" => substr(md5(mt_rand(0,10)),0,3),
                "document_number" => substr(md5(mt_rand(0,10)),0,10),
                "transaction_headers_id" => $th->id,
                "product_code" => $key,
                "price" => $data->price,
                "quantity" => $r->kuantiti[$i],
                "unit" => $data->unit,
                "sub_total" => ($data->price * $r->kuantiti[$i]),
                "currency" => "IDR",
            ]);
        }
        return view('checkout/all', compact('data'));
    }
    public function report()
    {
        $data = TransactionHeader::with('detail')->get();
        return view('checkout.report', compact('data'));
    }
}
