<?php

namespace App\Http\Controllers\Repository;

use Auth;
use \App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserRepository extends Controller
{
    public static function index(Request $r, $state)
    {
        $r["numStatus"] = 0;
        if($state == "done"){
            $r["numStatus"] = 1;
        }else if($state == "reject"){
            $r["numStatus"] = 2;
        }
        $user = Auth::user();
        $collect = User::select('id','status','name','nik','kecamatan_id','user_id')->has('kec.dapil')->with('kec.dapil')->where('role','!=','admin')
        ->when(is_numeric($r->s) == 1, function($q) use ($r){
            return $q->where('nik','like','%'.$r->s.'%')->whereStatus($r["numStatus"]);
        })->when(is_numeric($r->s) != 1, function($q) use ($r){
            return $q->where('name','like','%'.$r->s.'%')->whereStatus($r["numStatus"]);
        })->when($state, function($q) use ($r){
            return $q->whereStatus($r["numStatus"]);
        })->when($user->role == 'korcab', function($q) use ($user){
            return $q->whereKotaId($user->kota_id);
        })->when($user->role == 'korpil', function($q) use ($user){
            return $q->whereHas('kec', function($qq) use ($user){
                return $qq->whereDapilId($user->kec->dapil_id);
            });
        })->when($user->role == 'korcam', function($q) use ($user){
            return $q->whereKecamatanId($user->kecamatan_id)->whereUserId($user->id);
        })->latest()->paginate(99999);
        return $collect;
    }
    public static function generatorUserData(Request $r, $state)
    {
        $user = Auth::user();
        $collect = User::has('kec.dapil')->has('kota')->has('kec')->has('desa')->has('program')->with('kec.dapil')->with('kota')->with('kec')->with('desa')->where('role','!=','admin')
        ->when($r->s, function($q) use ($r){
            return $q->where('name','like','%'.$r->s.'%')->orWhere('nik','like','%'.$r->s.'%')->orWhere('nohp','like','%'.$r->s.'%');
        })
        ->when($state == "wait", function($q){
            return $q->whereStatus(0);
        })->when($state == "done", function($q){
            return $q->whereStatus(1);
        })->when($state == "reject", function($q){
            return $q->whereStatus(2);
        })->paginate(99999);
        return $collect;
    }
    public static function detailUser($id)
    {
        $data = User::whereId($id)->with('kec.dapil')->with('desa')->with('kota')->with('program')->first();
        return $data;
    }
}
