<?php

namespace App\Http\Controllers\Repository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FCM extends Controller
{
    static public function send($title,$msg,$fcm)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $apiKey = "AAAAv8F9AUQ:APA91bHaxntxmHkHjSSaN41gHLSlji2WCvUAgmes3jgZm140yeNuS3G42CBO23nvqPk7Z9gpvNvQAg6fsGVx9IlwrygleVffAtLfpfRSCVwWeExs2T8ALBj3IbAA1ByDBQv28v8KbNYd";
        $headers = array (
            'Authorization:key=' . $apiKey,
            'Content-Type:application/json'
        );
        $dataPayload = ['to'=> 'My Name', 
            'points'=>80, 
            'other_data' => 'This is extra payload'
        ];
        $apiBody = [
            "notification" => [
                "title" => $title,
                "body" => $msg,
            ],
            "data" => [
                "title" => $title,
                "body" => $msg,
            ],
            "to" => $fcm,
        ];
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($apiBody));
        $result = curl_exec($ch);
        print($result);
        curl_close($ch);
        return $result;
    }
}
