<?php

namespace App\Http\Controllers\Api;

use Auth;
use \App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\TransactionHeader;
use \App\TransactionDetail;

class GlobalController extends Controller
{
    public function getProduct()
    {
        $data['product'] = Product::All();
        return $data;
    }
    public function checkout(Request $r)
    {
        $th = TransactionHeader::create([
            'document_Code' => substr(md5(mt_rand(0,10)),0,3),
            'document_Number' => substr(md5(mt_rand(0,10)),0,10),
            'total' => $r->total,
            'user' => Auth::user()->id,
            'date' => \Carbon\Carbon::now(),
        ]);
        $idProd = json_decode($r->productCode, true);
        $qty = json_decode($r->qty, true);
        foreach ($idProd as $i => $key) {
            $data = Product::whereId($key)->first();
            TransactionDetail::create([
                "document_code" => substr(md5(mt_rand(0,10)),0,3),
                "document_number" => substr(md5(mt_rand(0,10)),0,10),
                "transaction_headers_id" => $th->id,
                "product_code" => $data->product_code,
                "price" => $data->price,
                "quantity" => $qty[$i],
                "unit" => $data->unit,
                "sub_total" => ($data->price * $qty[$i]),
                "currency" => "IDR",
            ]);
        }
    }
}
