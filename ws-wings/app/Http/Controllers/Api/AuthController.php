<?php

namespace App\Http\Controllers\Api;

use Auth;
use \App\User;
use \App\Program;
use Validator;
use \App\Dapil;
use \App\Desa;
use \App\Nikcheck;
use \App\Kecamatan;
use \App\ApprovaltUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Controllers\Repository\FCM;
use \App\Http\Controllers\Repository\UserRepository;

class AuthController extends ValidatorController
{
    public $ok = 203;

    public function update($type="profile", Request $r)
    {
        if ($type=='pass') {
            $data = User::whereId(Auth::user()->id)->update(['password' => bcrypt($r->password)]);
            return response()->json($data);
        }else if ($type=='profile') {
            $dataUpdate = ["name"=>$r["name"],"email"=>$r["email"],"nik"=>$r["nik"],"nohp"=>$r["nohp"],"alamat"=>$r["alamat"]];
            if ($r['ktp'] != "X") {
                $r['ktp'] = $this->base64Compresser('data:image/jpeg;base64,'.$r['ktp'], $this->ktpPath());
                $dataUpdate = ["name"=>$r["name"],"email"=>$r["email"],"nik"=>$r["nik"],"nohp"=>$r["nohp"],"alamat"=>$r["alamat"], "ktp" => $r["ktp"]];
            }
            $u = User::whereId($r["id"])->update($dataUpdate);
            return $u;
        }
    }
    public function detailUser($id)
    {
        $data = UserRepository::detailUser($id);
        return $data;
    }
    public function verif($state, Request $r)
    {
        $collect = UserRepository::index($r, $state);
        return $collect;
    }
    public function accUser($uid, $status, Request $r)
    {
        $dud = User::whereId($uid);
        $dad = $dud->with('parent')->first();
        if($dad->status == 0){
            $res = $dad->update(['status'=>$status,'role'=>$r->role]);
            $this->storeLogUserVerify($dad->id, $status,$r->desc);
            
            $title = "Verifikasi Akun";
            $msg = '';
            $msgCurrent = '';
            $fcmCurrent = $dad->fcm;
            if ($r->reason) {
                $msg = "Akun dengan nama " . $dad->name . " ditolak saat proses verifikasi";
                $msgCurrent = "Akun anda " . $dad->name . " ditolak saat proses verifikasi";
            }else{
                $msg = "Akun dengan nama " . $dad->name . " telah diverifikasi";
                $msgCurrent = "Akun anda " . $dad->name . " telah diverifikasi";
            }
            
            if ($dad->parent) {
                $fcm = $dad->parent->fcm;
                FCM::send($title,$msg,$fcm);
            }
            FCM::send($title,$msgCurrent,$fcmCurrent);
            return response()->json("Updated Status from Not Active to : " . $res);
        }else if ($dad->status == 1){
            $res["status"] = "verified";
            $res["response"] = $dud->with('approval')->first();
            return $res;
        }else if ($dad->status == 2){
            $this->storeLogUserVerify($dad->id, 0, 'Alasan penolakan adalah ' . $r->reason);
            return "AKUN DITOLAK";
        }
    }
    public function program(Request $r) // INI PROGRAM
    {
        $data = Program::get();
        return $data;
    }
    
    public function login(){
        $dm = ['email' => request('email'), 'password' => request('password')];
        if(Auth::attempt($dm)){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            $success['user'] = User::whereId($user->id)->when($user->status == 2, function($q){
                return $q->with('rejects');
            })->first();
            return response()->json(['status'=>'ok','msg' => $success], 203);
        }
        else{
            return response()->json(['status'=>'error','msg'=>'Kombinasi nik/email & password tidak sesuai'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), $this->registerApp());
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['ktp'] = $this->base64Compresser('data:image/jpeg;base64,'.$input['ktp'], $this->ktpPath());
        $input['password'] = bcrypt($input['password']);
        $success['user'] = User::whereId(User::create($input)->id)->first();
        $success['token'] = $success['user']->createToken('nApp')->accessToken;
        Nikcheck::updateOrCreate(['nik' => $input['nik']])->update(['used'=>true]);

        return response()->json(['status'=>'ok','msg' => $success], 203);
    }

    public function details(Request $r)
    {
        $user = Auth::user();
        $da = User::whereId($user->id)->first();
        return response()->json(['msg' => $da], 203);
    }
    public function checkUser(Request $r)
    {
        $data = User::whereNik($r->nik)->with('parent')->first();
        if ($data) {
            return response()->json(['success'=>$data], 203);
        }else{
            return response()->json(['success'=>'Nik pengguna belum terdaftar'], 401);
        }
    }
    public function childUser()
    {
        $data = User::whereId(Auth::user()->id)->with('child.program')->first();
        User::whereUserId(Auth::user()->id)->get();
        if ($data) {
            return response()->json(['data'=>$data], 203);
        }else{
            return response()->json(['success'=>'Nik pengguna belum terdaftar'], 401);
        }
    }
    public function nikCreate(Request $r)
    {
        if(Nikcheck::whereNik($r->nik)->first()){
           return response()->json(['success'=>'Nik pengguna sudah terdaftar'], 401);
        }
        $input = $r->all();
        $input["used"] = false;
        $data = Nikcheck::create($input);
        return response()->json(['success'=>$data], 203);
    }
}
