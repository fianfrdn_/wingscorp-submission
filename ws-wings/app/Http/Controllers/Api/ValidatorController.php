<?php

namespace App\Http\Controllers\Api;

use Auth;
use \App\ApprovaltUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ValidatorController extends Controller
{
    public function storeLogUserVerify($uid, $status, $desc)
    {
        $decs = $status == "1" ? "di approve" : "di tolak";
        $fields["user_id"] = $uid;
        $fields["admin_id"] = Auth::user()->id;
        $fields["val"] = "Akun telah ".$decs." saat proses verifikasi oleh " . Auth::user()->name .'. ' . $desc;
        $fields["status"] = $status == "2" ? "0" : "1";
        ApprovaltUser::create($fields);
    }
    public function ktpPath()
    {
        return public_path('user/ktp/');
    }
    public function registerApp()
    {
        $vl = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'nik' => 'required|min:16|max:16|unique:users',
            'nohp' => 'required|min:10|max:14',
            'ktp' => 'required',
            'alamat' => 'required',
            'program_id' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
        ];
        return $vl;
    }
    public function base64Compresser($val, $path)
    {
        $pathTo = $path;        
        $imgParts = explode(";base64,", $val);
        $mimes = explode("image/", $imgParts[0]);
        $imgType = $mimes[1];
        $based64 = base64_decode($imgParts[1]);
        $bch = $this->randNamed() . '.'.$imgType;
        $file = $pathTo . $bch;
        file_put_contents($file, $based64);
        return $bch;
    }
    private function randNamed()
    {
        $f = mt_rand(10000, 99999).'-'.mt_rand(100000000, 999999099).'-'.mt_rand(10000, 99999);
        return $f;
    }
}
