<?php

namespace App\Http\Controllers\Api;

use Auth;
use \App\Desa;
use \App\User;
use \App\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarDataController extends Controller
{
    public function simpulHome()
    {
        $data = [];
        $dud = User::has('program')->with('program')->get(['program_id'])->groupBy('program.name'); // GET SIMPUL DI AWAL HOME
        foreach ($dud as $hj => $kl) {
            $nml = 0;
            foreach ($kl as $key) {
                $nml++;
                $data[$hj]['dapil'] = $key->program_id;
                $data[$hj]['name'] = $hj;
                $data[$hj]['sum_user'] = $nml;
            }
        }
        return $data;
    }
    public function detailSimpul(Request $r)
    {
        $data = [];
        $inde = 'program';
        $inde = $r->type;
        if ($r->type=="kota") {
            $data = User::whereProgramId($r->program)->with('kota')->get(['kota_id','program_id'])->groupBy('kota.name'); 
        }
        else if ($r->type=="kec") {
            $data = User::whereProgramId($r->program)->whereKotaId($r->location)->with('kec')->get(['kecamatan_id','kota_id'])->groupBy('kec.name'); 
        }
        else if ($r->type=="desa") {
            $data = User::whereProgramId($r->program)->whereKecamatanId($r->location)->with('desa')->get(['desa_id','kecamatan_id'])->groupBy('desa.name'); 
        }

        $ar = [];
        $lp = [];
        foreach ($data as $hj => $kl) {
            $nml = 0;
            foreach ($kl as $key) {
                $nml++;
                $lp[$hj]['dapil'] = $key[$r->type]["id"];
                $lp[$hj]['name'] = $hj;
                $lp[$hj]['user_count'] = $nml;
            }
            array_push($ar, $lp[$hj]);
        }
        $typeSimpul = ["kota","kec","desa","end"];
        $idxArray = array_search($r->type, $typeSimpul);
        $dataBaru["data"] = $ar;
        $dataBaru["type"] = $typeSimpul[$idxArray+1];
        return $dataBaru;
    }

    
    public function tus(Request $r) // INI KEC
    {
        $data = [];
        if ($r->kip == 1) {
            $data = Kecamatan::whereDapilId($r->dapil)->whereHas('userKip')->withCount('userKip')->get();
        }else{
            $user = Auth::user();
            if ($user->role == 'korcam') {
                $data = Kecamatan::whereId($user->kecamatan_id)->whereDapilId($r->dapil)->whereHas('user')->withCount('user')->get();                
            }else{
                $data = Kecamatan::whereDapilId($r->dapil)->whereHas('user')->withCount('user')->get();
            }
        }
        return $data;
    }
    public function tis(Request $r) // INI DESA
    {
        $data = [];
        if ($r->kip == 1) {
            $data = Desa::whereDistrictId($r->kec)->whereHas('userKip')->withCount('userKip')->get();
        }else{
            $data = Desa::whereDistrictId($r->kec)->withCount('user')->having('user_count','>',0)->get();
        }
        return $data;
    }
    public function tes(Request $r) // INI DAPIL KAB
    {
        $data = [];
        // $counts = $r->kip == 1 ? "user_kip_count" : "user_count";
        $counts = "user_count";
        $user = Auth::user();
        // $data = Kecamatan::when($user->role == 'korpil', function($q) use ($user){
        //     return $q->whereDapilId($user->kec->dapil_id);
        // })->when($user->role == 'korcam', function($q) use ($user){
        //     return $q->whereId($user->kecamatan_id);
        // })->whereRegencyId($r->kota)->when($r->kip == 1, function($q){
        //     return $q->has('userKip')->withCount('userKip');
        // })->when($r->kip != 1, function($q){
        //     return $q->has('user')->withCount('user');
        // })->get()->makeHidden(['id','regency_id'])->groupBy('dapil.name');

        $data = Kecamatan::whereRegencyId($r->kota)->has('user')->withCount('user')->get()->makeHidden(['id','regency_id'])->groupBy('dapil.name');

        $lp = [];
        foreach ($data as $key => $value) {
            $num = 0;
            foreach ($data[$key] as $key2) {
                if( $key2->dapil_id != null){
                    $lp[$key]["sum_user"] = ($num+=$key2[$counts]);
                    $lp[$key]["dapil"] = $key2->dapil_id;
                }
            }
            
        }
        return collect($lp)->sortBy(function($x, $key){
            return $x['dapil'];
        });
    }
}
// if ($user->role == 'korpil') {
            //     $data = Kecamatan::whereDapilId($user->kec->dapil_id)->whereRegencyId($r->kota)->whereHas('user')->withCount('user')->get()->makeHidden(['id','regency_id'])->groupBy('dapil.name');
            // }else if ($user->role == 'korcam') {
            //     $data = Kecamatan::whereId($user->kecamatan_id)->whereRegencyId($r->kota)->whereHas('user')->withCount('user')->get()->makeHidden(['id','regency_id'])->groupBy('dapil.name');
            // }else{
            //     $data = Kecamatan::whereRegencyId($r->kota)->whereHas('user')->withCount('user')->get()->makeHidden(['id','regency_id'])->groupBy('dapil.name');                
            // }