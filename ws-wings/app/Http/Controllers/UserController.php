<?php

namespace App\Http\Controllers;

use Auth;
use \App\User;
use \App\ApprovaltUser;
use \App\Http\Controllers\Repository\UserRepository;
use \App\Http\Controllers\Repository\FCM;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function kecamatan($kota)
    {
        // $path = public_path() . "/location/kecamatan.json";
        // $json = json_decode(file_get_contents($path), true); 
        // $data = collect($json)->filter(function($js){
        //     return $js['regency_id'] == 3500;
        // });
        $json = \App\Kecamatan::whereRegencyId($kota)->get(['id','name']);
        return $json;
    }
    public function desa($kecamatan)
    {
        // $path = public_path() . "/location/kecamatan.json";
        // $json = json_decode(file_get_contents($path), true); 
        // $data = collect($json)->filter(function($js){
        //     return $js['regency_id'] == 3500;
        // });
        $json = \App\Desa::whereDistrictId($kecamatan)->get(['id','name']);
        return $json;
    }
    public function index(Request $r)
    {
        $state = 'wait';
        if ($r->state != null) {
            $state = $r->state;
        }
        $collect = UserRepository::index($r, $state);
        return view('doc/admin/user/index', compact('collect'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kota'] = \App\Kota::All();
        $data['program'] = \App\Program::All();
        return view('doc/admin/user/create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // return $r;
        $in = $r->all();
        $file = $r->file('ktp');
        $tujuan_upload = 'user/ktp/';
        $file->move($tujuan_upload,$file->getClientOriginalName());
        $in['password'] = bcrypt(substr(md5($in['email']), 0, 6));
        $in['ktp'] = $file->getClientOriginalName();
        $in['status'] = 1;
        $in['user_id'] = Auth::id();
        User::create($in);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailUser($id)
    {
        $data = UserRepository::detailUser($id);
        return view('doc/admin/user/detail', compact('data'));
        // return $data;
    }
    public function verif(Request $r)
    {
        $updateData = ['status' => $r->status,'role' => $r->role];
        if ($r->status == 2) {
            $updateData = ['status' => $r->status];
            ApprovaltUser::create([
                "user_id" => $r->id,
                "admin_id" => Auth::id(),
                "val" => $r->desc,
                "status" => '0',
            ]);
        }
        User::whereId($r->id)->update($updateData);
        
        $dud = User::whereId($r->id);
        $dad = $dud->with('parent')->first();
        $title = "Verifikasi Akun";
        $msg = '';
        $msgCurrent = '';
        $fcmCurrent = $dad->fcm;
        if ($r->reason) {
            $msg = "Akun dengan nama " . $dad->name . " ditolak saat proses verifikasi";
            $msgCurrent = "Akun anda " . $dad->name . " ditolak saat proses verifikasi";
        }else{
            $msg = "Akun dengan nama " . $dad->name . " telah diverifikasi";
            $msgCurrent = "Akun anda " . $dad->name . " telah diverifikasi";
        }
        
        if ($dad->parent) {
            $fcm = $dad->parent->fcm;
            FCM::send($title,$msg,$fcm);
        }
        FCM::send($title,$msgCurrent,$fcmCurrent);
        return redirect()->back();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
