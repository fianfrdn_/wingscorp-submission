<?php

namespace App\Http\Controllers;

use \App\Desa;
use \App\Dapil;
use \App\Kecamatan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $r)
    {
        // $data = \App\User::has('program')->with('program')->get(['program_id'])->groupBy('program.name'); // GET SIMPUL DI AWAL HOME

        $data = \App\User::whereProgramId($r->program)->with('kota')->get(['kota_id'])->groupBy('kota.name'); 
        $lp = [];
        foreach ($data as $hj => $kl) {
            $nml = 0;
            foreach ($kl as $key) {
                $nml++;
                $lp[$hj]['id'] = $key->kota_id;
                $lp[$hj]['sum_user'] = $nml;
            }
        }
        // print_r($lp);
        // GET DETAIL BY SIMPUL ID        
        return $lp;
    }

    public function tester()
    {
        $data["kecamatan"] = Kecamatan::get()->groupBy('regency_id');
        return $data;
    }
}
