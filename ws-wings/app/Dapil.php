<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dapil extends Model
{
    protected $table = 'dapil';
    protected $guarded = [];
    public $timestamps = false;
    protected $hidden = ['created_at','updated_at'];

    public function kecamatan()
    {
        return $this->hasMany('App\Kecamatan','dapil_id');
    }
    public function kecamatanUser()
    {
        return $this->hasMany('App\Kecamatan','dapil_id')->whereHas('user');
    }
}
