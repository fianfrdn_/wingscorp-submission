<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $table = 'desa';
    protected $guarded = [];
    public $timestamps = false;
    // protected $appends = array('sumUser');

    public function kec()
    {
        return $this->belongsTo('App\Kecamatan','district_id');
    }
    public function user()
    {
        return $this->hasMany('App\User','desa_id');
    }
    public function userKip()
    {
        return $this->hasMany('App\User','desa_id')->where('program_id',1);
    }
    // public function getSumUserAttribute()
    // {
    //     return $this->user()->count();
    // }
}
