<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nikcheck extends Model
{
    protected $table = 'nikcheck';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
}
