<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHeader extends Model
{
    protected $guarded = [];

    public function detail()
    {
        return $this->hasMany('App\TransactionDetail','transaction_headers_id');
    }
    public function userData()
    {
        return $this->belongsTo('App\User','user');
    }
}
