<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $appends = array('fixPrice');

    public function getFixPriceAttribute()
    {
        return $this->attributes['price'] * ((100 - $this->attributes['discount']) / 100);  
    }
}
