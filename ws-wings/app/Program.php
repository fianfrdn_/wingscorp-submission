<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'program';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];

    public function user()
    {
        return $this->hasMany('App\User','program_id');
    }
}
