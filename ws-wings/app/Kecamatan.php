<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $guarded = [];
    public $timestamps = false;

    public function kab()
    {
        return $this->belongsTo('App\Kota','regency_id');
    }
    public function user()
    {
        return $this->hasMany('App\User','kecamatan_id');
    }
    public function userKip()
    {
        return $this->hasMany('App\User','kecamatan_id')->where('program_id',1);
    }
    public function dapil()
    {
        return $this->belongsTo('App\Dapil','dapil_id');
    }
}
