<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovaltUser extends Model
{
    protected $table = 'approval_user';
    protected $fillable = ['user_id','admin_id','val','status'];
    protected $hidden = ['created_at','updated_at'];
}
