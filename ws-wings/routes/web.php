<?php

Route::get('/tes','HomeController@tester');


Route::group(['middleware' => ['auth']], function () {
	Route::get('/', 'WebController@checkout')->name('home');
	Route::get('/checkout', 'WebController@checkout')->name('checkout');
	Route::get('/store1', 'WebController@store1')->name('checkout.store1');
	Route::get('/store2', 'WebController@store2')->name('checkout.store2');
	Route::get('/store3', 'WebController@store3')->name('checkout.store3');
	Route::get('/report', 'WebController@report')->name('report');
	Route::get('/home', 'WebController@checkout')->name('home');
});
Auth::routes();


// Route::get('/u')
