<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['as' => 'api.', 'namespace' => 'Api'], function () {
    Route::prefix('UO5E33SE2I-WT62CP6WAP-WHVPYH10E6')->group(function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('me', 'AuthController@details');
            Route::get('product', 'GlobalController@getProduct');
            Route::post('checkout', 'GlobalController@checkout');
        });
    });
});