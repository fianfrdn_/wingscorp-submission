<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('document_code',3);
            $table->string('document_number',10);
            $table->unsignedBigInteger('transaction_headers_id')->references('id')->on('transaction_headers');
            $table->string('product_code');
            $table->integer('price');
            $table->integer('quantity');
            $table->string('unit',5);
            $table->integer('sub_total');
            $table->string('currency',5);
            $table->timestamps();
            $table->foreign('transaction_headers_id')->references('id')->on('transaction_headers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
