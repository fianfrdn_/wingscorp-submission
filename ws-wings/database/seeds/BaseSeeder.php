<?php

use \App\User;
use \App\Product;
use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100; $i++) { 
            User::create([
                'name'=> 'Pengguna ' . $i,
                'email'=> 'pengguna'.$i,
                'password'=> bcrypt('p'),
            ]);
        }
        for ($i=1; $i < 10; $i++) { 
            Product::create([
                'product_code' => substr(strtoupper(md5(mt_rand(0,9999))), 0,18),
                'product_name' => 'Product ' . $i,
                'currency' => 'IDR',
                'price' => mt_rand(0,50) . 10000,
                'discount' => mt_rand(0,50),
                'dimension' => mt_rand(10,50) . ' x ' . mt_rand(10,50) .' cm',
                'unit' => 'PCS',
            ]);
        }
    }
}
