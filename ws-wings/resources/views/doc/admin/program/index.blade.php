@extends('root.std')
@section('title', 'Data Program')
@section('menu', 'Program')
@section('content')

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{route('program.create')}}" method="POST">
  	@csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Data Program</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="form-group">
      		<label for="text">Nama Program</label>
      		<input type="text" class="form-control" name="name">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-dark btn-sm">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>

<div class="col-12 mt-3">
  <div class="card card-body">
  	<div class="row justify-content-end mb-3">
  		<button class="btn btn-sm btn-dark col-3 mb-2" data-toggle="modal" data-target="#exampleModal" type="button">Input Program</button>
  	</div>
    <table border="1" class="table table-stripped table-bordered text-center">
		<thead>
			<th width="1" class="text-center">No</th>
			<th class="text-left">Nama </th>
		</thead>
		<tbody>
			@foreach($data as $i => $d)
			<tr>
				<td>{{$i+1}}</td>
				<td class="text-left">{{$d->name}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
  </div>
</div>
@endsection

