@extends('root.std')
@section('title', 'Data Dapil')
@section('menu', 'Data Dapil')
@section('content')

<div class="col-12 mt-3">
  <div class="card card-body">
    <table border="1" class="table table-stripped table-bordered text-center">
		<thead>
			<th width="1" class="text-center">No</th>
			<th class="text-center">Nama {{$state}}</th>
			<th class="text-center">Total Pengguna</th>
			@if($state != 'Desa')
			<th class="text-center">#</th>
			@endif
		</thead>
		<tbody>
			@foreach($data as $i => $d)
			<tr>
				<td>{{$i+1}}</td>
				<td>{{$d->name}}</td>
				<td>{{$d->user_count}}</td>
				@if($state != 'Desa')
				<td><a href="{{route('dapil.tis', ['kec' => $d->id])}}" class="btn btn-sm btn-dark p-0 pl-2 pr-2">Detail</a></td>
				@endif
			</tr>
			@endforeach
		</tbody>
	</table>
	<canvas id="myChart" height="300"></canvas>
  </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js" integrity="sha512-ElRFoEQdI5Ht6kZvyzXhYG9NqjtkmlkfYk0wr6wHxU9JEHakS7UJZNeml5ALk+8IKlU6jDgMabC3vkumRokgJA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
var data = @json($data);
var hah = [];
var hat = [];
data.forEach(function(post) {
	hat.push(post['name']);
	hah.push(post['user_count']);
});
const ctx = document.getElementById('myChart');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: hat,
        datasets: [{
            data: hah,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1
        }]
    },
});
</script>

@stop
