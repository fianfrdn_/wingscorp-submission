@extends('root.std')
@section('title', 'Data Dapil')
@section('menu', 'Data Dapil')
@section('content')

<div class="col-12 mt-3">
  <div class="card card-body">
  	<div class="row justify-content-end mb-3">
  		<a href="{{route('dapil.index',['kota' => 3509])}}" class="col-2 btn btn-sm btn-dark">Jember</a>
	  	<a href="{{route('dapil.index',['kota' => 3508])}}" class="ml-1 col-2 btn btn-sm btn-dark">Lumajang</a>
	  	<a href="{{route('dapil.simpul')}}" class="ml-2 col-2 btn btn-sm btn-dark">Simpul</a>
  	</div>
    <table border="1" class="table table-stripped table-bordered text-center">
		<thead>
			<th width="1" class="text-center">No</th>
			<th class="text-center">
				@if($ret == 'dapil')
				Lokasi Dapil
				@else
				Program
				@endif
			</th>
			<th class="text-center">Total Pengguna</th>
			<th class="text-center">#</th>
		</thead>
		<tbody>
@if($ret == 'dapil')
			@php
			$no = 0;
			$arName = [];
			$arValue = [];
			@endphp
			@foreach ($m as $i => $a)
			@php
			$no++;
			array_push($arName, $i);
			array_push($arValue, $a["sum_user"]);
			@endphp
				<tr>
					<td>{{$no}}</td>
					<td>{{$i}}</td>
					<td>{{$a["sum_user"]}}</td>
					<td>
						<a href="{{route('dapil.tus', ['dapil' => $a["dapil"]])}}" class="btn btn-sm btn-dark">Detail Dapil</a>
					</td>
				</tr>
			@endforeach
@else
			@php
			$no = 0;
			$arName = [];
			$arValue = [];
			@endphp
			@foreach ($data as $i => $a)
			@php
			$no++;
			array_push($arName, $i);
			array_push($arValue, $a["sum_user"]);
			@endphp
				<tr>
					<td>{{$no}}</td>
					<td>{{$i}}</td>
					<td>{{$a["sum_user"]}}</td>
					{{-- <td>{{$a["dapil"]}}</td> --}}
					<td>
						<a href="" class="btn btn-sm btn-dark">Detail Dapil</a>
					</td>
				</tr>
			@endforeach
@endif
		</tbody>
	</table>
	<canvas id="myChart" height="300"></canvas>
  </div>
</div>
@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js" integrity="sha512-ElRFoEQdI5Ht6kZvyzXhYG9NqjtkmlkfYk0wr6wHxU9JEHakS7UJZNeml5ALk+8IKlU6jDgMabC3vkumRokgJA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
var dataValue = @json($arValue);
var dataName = @json($arName);
var hah = [];
var hat = [];
dataName.forEach(function(post) {
	hat.push(post);
});
dataValue.forEach(function(post) {
	hah.push(post);
});
const ctx = document.getElementById('myChart');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: hat,
        datasets: [{
            data: hah,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
            ],
        }],

    },
});
</script>

@stop