@extends('root.std')
@section('title', 'Data Pengguna')
@section('menu', 'Data Pengguna')
@section('content')
@php
$param = '';
if (array_key_exists("QUERY_STRING", $_SERVER)) {
  $param = $_SERVER['QUERY_STRING'];
}
@endphp
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{route('program.create')}}" method="POST">
    @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Filter Data Pengguna</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="text">Berdasar Dapil</label>
          <input type="text" class="form-control" name="name">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-dark btn-sm">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>


<div class="col-12 mt-3">
  {{-- <div class="row justify-content-end mb-3 p-0 m-0">     --}}
    {{-- <button class="btn btn-dark col-3" data-toggle="modal" data-target="#exampleModal" type="button">Filter Data Pengguna</button> --}}
  {{-- </div> --}}

  <div class="card card-body">
  	<div class="row justify-content-between mb-3 p-0 m-0">
      <div class="row col-4">
          <a class="btn btn-sm btn-dark col-2" href="{{'generator/pdf?'.$param}}">PDF</a>
          <a class="btn btn-sm btn-dark col-2 ml-1" href="{{'generator/excell?'.$param}}">Excell</a>
      </div>

      <div class="row justify-content-end col-8 p-0 m-0">        
        <a class="btn btn-sm btn-dark col-3 mr-1" href="{{route('user.index', ['state' =>'done'])}}">Terverifikasi</a>
        <a class="btn btn-sm btn-dark col-3 mr-1" href="{{route('user.index', ['state' =>'wait'])}}">Menunggu Verifikasi</a>
        <a class="btn btn-sm btn-dark col-3" href="{{route('user.index', ['state' =>'reject'])}}">Verifikasi Ditolak</a>
      </div>
  	</div>
    <table border="1" class="table table-stripped table-bordered text-center">
		<thead>
			<th width="1" class="text-center">No</th>
      <th class="text-center">Nama Pendaftar</th>
      <th class="text-center">NIK</th>
      <th class="text-center">Dapil</th>
      <th>#</th>
		</thead>
		<tbody>
			@foreach ($collect as $i => $a)
       <tr>
         <td>{{$i+1}}</td>
         <td>{{$a->name}}</td>
         <td>{{$a->nik}}</td>
         <td>{{$a->kec->dapil->name}}</td>
         <td><a class="btn btn-sm btn-dark" href="{{route('user.detail', ['id' => $a->id])}}">Detail</a></td>
       </tr> 
      @endforeach
		</tbody>
	</table>
  </div>
</div>
@endsection

