@extends('root.std')
@section('title', 'Input Pengguna')
@section('menu', 'Input Pengguna')
@section('content')

<div class="col-12 mt-3">
  <div class="card card-body">
  <form action="{{route('user.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <table class="table table-bordered">
      @include('doc/widget/lr-input', ['l'=>'name', 'type'=>'text','name'=>'name'])
      @include('doc/widget/lr-input', ['l'=>'email', 'type'=>'email','name'=>'email'])
      @include('doc/widget/lr-input', ['l'=>'password', 'type'=>'text','name'=>'password'])
      @include('doc/widget/lr-input', ['l'=>'nik', 'type'=>'text','name'=>'nik'])
      @include('doc/widget/lr-input', ['l'=>'nohp', 'type'=>'text','name'=>'nohp'])
      @include('doc/widget/lr-input', ['l'=>'alamat', 'type'=>'text','name'=>'alamat'])
      @include('doc/widget/lr-input', ['l'=>'ktp', 'type'=>'file','name'=>'ktp'])
      <tr>
        <td>Role</td>
        <td>
          <select name="role" class="form-control">
            <option selected disabled>Pilih role</option>
            <option value="admin">{{strtoupper("admin")}}</option>
            <option value="korcab">{{strtoupper("korcab")}}</option>
            <option value="korpil">{{strtoupper("korpil")}}</option>
            <option value="korcam">{{strtoupper("korcam")}}</option>
            <option value="user">{{strtoupper("user")}}</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Kota</td>
        <td>
          <select id="kota_id" name="kota_id" class="form-control">
            <option selected disabled>Pilih Kota</option>
            @foreach($data['kota'] as $city)
            <option value="{{$city->id}}">{{$city->name}}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <td>Kecamatan</td>
        <td>
          <select id="kecamatan_id" name="kecamatan_id" class="form-control">
            <option selected disabled>Pilih Kecamatan</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Desa</td>
        <td>
          <select id="desa_id" name="desa_id" class="form-control">
            <option selected disabled>Pilih Desa</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Program</td>
        <td>
          <select name="program_id" class="form-control">
            <option selected disabled>Pilih Program</option>
            @foreach($data['program'] as $city)
            <option value="{{$city->id}}">{{$city->name}}</option>
            @endforeach
          </select>
        </td>
      </tr>
		</tbody>
	</table>
  <div class="row justify-content-end">
    <button class="btn btn-dark col-3 mt-4 mb-4" type="submit">Buat Data Pengguna</button>
  </div>
  </div>
  </form>
</div>
@endsection
@section('js')
  <script>
    $("#kota_id").on('change', function(){
      // alert($(this).val());
      $.ajax({
        method : "GET",
        url : "{{url('pengguna/kecamatan/')}}/" + $(this).val(),
        success : function(res){
          $.each(res, function(index, item){
            $("#kecamatan_id").append(`<option value="`+item.id+`">`+item.name+`</option>`);
          })
        }
      })
    });
    $("#kecamatan_id").on('change', function(){
      $.ajax({
        method : "GET",
        url : "{{url('pengguna/desa/')}}/" + $(this).val(),
        success : function(res){
          $.each(res, function(index, item){
            $("#desa_id").append(`<option value="`+item.id+`">`+item.name+`</option>`);
          })
        }
      })
    });
  </script>
@endsection