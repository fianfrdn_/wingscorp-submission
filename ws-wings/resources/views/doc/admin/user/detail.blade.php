@extends('root.std')
@section('title', 'Data Pengguna')
@section('menu', 'Data Pengguna')
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{route('user.verif')}}" method="POST">
    @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Verifikasi Pengguna</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="text">Level Role Pengguna</label>
          <input type="hidden" name="status" value="1">
          <input type="hidden" name="id" value="{{$data->id}}">
          <select name="role" class="form-control">
            <option selected disabled>Pilih role</option>
            <option value="admin">{{strtoupper("admin")}}</option>
            <option value="korcab">{{strtoupper("korcab")}}</option>
            <option value="korpil">{{strtoupper("korpil")}}</option>
            <option value="korcam">{{strtoupper("korcam")}}</option>
            <option value="user">{{strtoupper("user")}}</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-dark btn-sm">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>

<div class="modal fade" id="tolakModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form action="{{route('user.verif')}}" method="POST">
    @csrf
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tolak Verifikasi Pengguna</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="text">Alasan Penolakan</label>
          <input type="hidden" name="status" value="2">
          <input type="hidden" name="id" value="{{$data->id}}">
          <textarea name="desc" class="form-control" cols="30" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-dark btn-sm">Simpan</button>
      </div>
    </div>
    </form>
  </div>
</div>

<div class="col-12 mt-3">
  <div class="card card-body">
    <div class="row justify-content-end p-0 m-0 mb-4">
      <button data-toggle="modal" data-target="#tolakModal" type="button" class="btn btn-sm btn-outline-dark pl-4 pr-4 mr-1">Tolak Pengguna</button>
      <button data-toggle="modal" data-target="#exampleModal" type="button" class="btn btn-sm btn-dark pl-4 pr-4">Verifikasi Pengguna</button>
    </div>
  	<table class="table table-bordered">
      @include('doc/widget/lr', ['l'=>'Nama', 'r'=> $data->name])
      @if($data->status == 0)
      @include('doc/widget/lr', ['l'=>ucfirst('Status Verifikasi'), 'r'=> "Menunggu Verifkasi"])
      @elseif($data->status == 1)
      @include('doc/widget/lr', ['l'=>ucfirst('Status Verifikasi'), 'r'=> "Terverifikasi"])
      @elseif($data->status == 2)
      @include('doc/widget/lr', ['l'=>ucfirst('Status Verifikasi'), 'r'=> "Verifikasi Ditolak"])
      @include('doc/widget/lr', ['l'=>ucfirst('Deskripsi Penolakan'), 'r'=> $data->rejects->val])

      @endif
      @include('doc/widget/lr', ['l'=>ucfirst('email'), 'r'=> $data->email])
      @include('doc/widget/lr', ['l'=>ucfirst('nik'), 'r'=> $data->nik])
      @include('doc/widget/lr', ['l'=>ucfirst('nohp'), 'r'=> $data->nohp])
      @include('doc/widget/lr', ['l'=>ucfirst('kecamatan'), 'r'=> $data->kec->name])
      @include('doc/widget/lr', ['l'=>ucfirst('dapil'), 'r'=> $data->kec->dapil->name])
      @include('doc/widget/lr', ['l'=>ucfirst('desa'), 'r'=> $data->desa->name])
      @include('doc/widget/lr', ['l'=>ucfirst('kota'), 'r'=> $data->kota->name])
      @include('doc/widget/lr', ['l'=>ucfirst('program'), 'r'=> $data->program->name])
      @include('doc/widget/lr', ['l'=>ucfirst('alamat'), 'r'=> $data->alamat])
      @include('doc/widget/lr', ['l'=>ucfirst('ktp'),'r'=>"X", 'img'=> $data->ktp])      
    </table>
  </div>
</div>
@endsection

