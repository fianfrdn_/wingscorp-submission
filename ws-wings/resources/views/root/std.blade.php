<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="{{asset('tmp/plugins/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('tmp/dist/css/adminlte.min.css')}}">
@yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button">
              <i class="fas fa-bars"></i>
            </a>
        </li>
      </ul>
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="{{asset('tmp/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">{{ucfirst(Auth::user()->name)}}</a>
          </div>
        </div>
        <div class="form-inline">
          <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div>
        </div>
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          @include('root/menu', $menus = [
            ['isBreak' => true,'menu' => 'MAIN MENU'],
            ['isBreak' => false,'icon'=>'nav-icon fas fa-th','menu' => 'Checkout','route' => 'checkout'],
            ['isBreak' => false,'icon'=>'nav-icon fas fa-th','menu' => 'Report','route' => 'report'],

          ])
        <li class="nav-item"><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="nav-link"><i class="fa fa-sign-out"></i><p>KELUAR</p><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form></a></li>
          </ul>
        </nav>
      </div>
    </aside>
    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">@yield('title')</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Main Menu</a></li>
                <li class="breadcrumb-item active">Dashboard v1</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <section class="content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </section>
    </div>
  </div>
  <script src="{{asset('tmp/plugins/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('tmp/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('tmp/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('tmp/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
  <script src="{{asset('tmp/dist/js/adminlte.js')}}"></script>
  @yield('js')
</body>
</html>