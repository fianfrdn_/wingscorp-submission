@foreach ($menus as $data)
@if($data['isBreak'])
 <li class="nav-header">{{$data["menu"]}}</li>
@else
<li class="nav-item">
  <a href="{{route($data['route'])}}" class="nav-link {{$__env->yieldContent('menu') == $data["menu"] ? "active" : ""}}">
    <i class="{{$data['icon']}}"></i>
    <p>{{$data["menu"]}}</p>
  </a>
</li>
@endif
@endforeach