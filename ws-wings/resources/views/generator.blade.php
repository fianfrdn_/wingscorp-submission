<!DOCTYPE html>
<html>
	<head>
		<title>PDF - SidabesJelas.com</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<table class='table table-bordered'>
			<thead>
				<th>No</th>
				<th>Nama</th>
				<th>NoHP</th>
				<th>NIK</th>
				<th>Alamat</th>
			</thead>
			<tbody>
				@foreach($user as $i => $u)
				<tr>
					<td>{{$i+1}}</td>
					<td>{{$u->name}}</td>
					<td>{{$u->nohp}}</td>
					<td>{{$u->nik}}</td>
					<td>{{$u->alamat}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</body>
</html>