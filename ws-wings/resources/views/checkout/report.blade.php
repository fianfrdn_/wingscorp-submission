@extends('root.std')
@section('title', 'Data Pengguna')
@section('menu', 'Data Pengguna')
@section('content')
<div class="col-12 mt-3">
  <div class="card card-body">
    <table class="table table-bordered">
      <thead>
        <th>Transaction</th>
        <th>User</th>
        <th>Total</th>
        <th>Date</th>
        <th>Item</th>
      </thead>
      <tbody>
        @foreach($data as $x)
        <tr>
          <td>{{strtoupper($x->document_Code)}}-{{strtoupper($x->document_Number)}}</td>
          <td>{{ucfirst($x->userData->name)}}</td>
          <td>Rp {{number_format($x->total)}}</td>
          <td>{{$x->date}}</td>
          <td>
            @foreach($x->detail as $d)
            {{$d->product->product_name}}
            <hr>
            @endforeach
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  var totalan = 0;
  function chg(val){  
      totalan += val;
      console.log(totalan);
  }
</script>
@stop