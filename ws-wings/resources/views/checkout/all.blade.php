@extends('root.std')
@section('title', 'Data Pengguna')
@section('menu', 'Data Pengguna')
@section('content')
<div class="col-12 mt-3">
  <div class="card card-body">
    <div class="row m-0 p-0 justify-content-center mt-3">
      <button class="btn {{$data["step"] == 1 ? "btn-dark" : "btn-outline-dark"}} col-4 mr-1">Pilih Produk</button>
      <button class="btn {{$data["step"] == 2 ? "btn-dark" : "btn-outline-dark"}} col-3 mr-1">Masukkan Kuantiti</button>
      <button class="btn {{$data["step"] == 3 ? "btn-dark" : "btn-outline-dark"}} col-4">Konfirmasi</button>
    </div>
    @if($data["step"] == 1)
    <form action="{{route('checkout.store1')}}" method="GET">
    @csrf
    <div class=" mt-5">
      @foreach($data["product"] as $p)
      <div class="row justify-content-between">
        <div class="row">
          <img src="https://dummyimage.com/hd92" class="img-thumbnail">
          <div class="mt-2 ml-2">
            <h5>{{$p->product_name}}</h5>
            @if($p->discount != 0)
            <h7 class="m-0 p-0"><s>{{$p->price}}</s></h7>
            @endif
            <h6 class="m-0 p-0">{{$p->fixPrice}}</h6>
          </div>
        </div>
        <div class="custom-control my-1 mr-sm-2">
          <input type="checkbox" class="form-control" id="customControlInline" value="{{$p->id}}" name="product_id[]">
        </div>
      </div>
      @endforeach
      <center><button type="submit" class="btn btn-dark m-0 mt-4 p-2 col-6">Submit</button></center>
    </div>
    </form>
    @elseif($data["step"] == 2)
    <form action="{{route('checkout.store2')}}" method="GET">
    @csrf
    <div class=" mt-5">
      @foreach($data["product"] as $p)
      <div class="row justify-content-between">
        <div class="row">
          <img src="https://dummyimage.com/hd92" class="img-thumbnail">
          <div class="mt-2 ml-2">
            <h5>{{$p->product_name}}</h5>
            @if($p->discount != 0)
            <h7 class="m-0 p-0"><s>{{$p->price}}</s></h7>
            @endif
            <h6 class="m-0 p-0">{{$p->fixPrice}}</h6>
          </div>
        </div>
        <div class="form-group">
          <label>Kuantiti</label>
          <input type="hidden" name="product_id[]" value="{{$p->id}}">
          <input name="kuantiti[]" value="0" type="number" class="form-control" onchange="chg(this.value * {{$p->fixPrice}})">
        </div>
      </div>
      @endforeach
      <center><button type="submit" class="btn btn-dark m-0 mt-4 p-2 col-6">Submit</button></center>
    </div>
    </form>
    @elseif($data["step"] == 3)
    <form action="{{route('checkout.store3')}}" method="GET">
    @csrf
    <center class="mt-5"><h2>Nota Pembayaran</h2></center>
      @php
      $totalPrice = 0;
      @endphp
      @foreach($data['product'] as $i => $p)
      @php
      $totalPrice = $data["kuantiti"][$i] * $p->fixPrice;
      @endphp
        <h4>{{$p->product_name}}</h4>
        <h6>Total : {{$data["kuantiti"][$i]}}</h6>
        <h6>Subtotal : IDR {{number_format($data["kuantiti"][$i] * $p->fixPrice)}}</h6>
        <hr>
        <input type="hidden" value="{{$p->product_code}}" name="prod_id[]">
        <input type="hidden" value="{{$data["kuantiti"][$i]}}" name="kuantiti[]">
      @endforeach
      <h2>Total Harga : IDR {{number_format($totalPrice)}}</h2>
      <input type="hidden" value="{{$totalPrice}}" name="total">
      <button type="submit" class="btn btn-dark mt-4 mb-2">Konfirmasi</button>
    </form>
    @endif
  </div>
</div>
@endsection

@section('js')
<script>
  var totalan = 0;
  function chg(val){  
      totalan += val;
      console.log(totalan);
  }
</script>
@stop